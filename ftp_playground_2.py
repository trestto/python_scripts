import pysftp
import os
import csv

cnopts = pysftp.CnOpts()
cnopts.hostkeys = None

with pysftp.Connection('192.168.25.74', username='root', password='opt!@#H7v', cnopts=cnopts) as sftp:
    # upload vm
    with sftp.cd('/opt/optimus/locutores/pt_BR-gs/527_prompts_v1'):
        directory = 'T:/Clientes/Projetos/527_2017_Intervalor_QuitAqui/Prompt/V.1/Tratados'
        for f in os.listdir(directory):
            sftp.put(os.path.join(directory,f), preserve_mtime=True)

    # download
    with sftp.cd('/opt/optimus/locutores/pt_BR-gs/527_prompts_v1'):
        os.chdir('C:/Users/augusto.nascimento/Documents/agente_virtual/locutores/pt_BR-gs/527_prompts_v1')
        for f in sftp.listdir():
            sftp.get(f, preserve_mtime=True)

with pysftp.Connection('192.168.25.58', username='root', password='opt!@#H7v', cnopts=cnopts) as sftp:
    # upload homologa
    with sftp.cd('/var/lib/asterisk/sounds/pt_BR-gs/527_prompts_v1'):
        directory = 'C:/Users/augusto.nascimento/Documents/agente_virtual/locutores/pt_BR-gs/527_prompts_v1'
        for f in os.listdir(directory):
            sftp.put(os.path.join(directory,f), preserve_mtime=True)
