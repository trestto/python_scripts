class EmailUser(object):
    """docstring for EmailUser."""
    def __init__(self, arg):
        super(EmailUser, self).__init__()
        self.user = 'rodolfo.dona@trestto.com.br'
        self.password = 'trestto@122'


database_list =  {
                    '200.201.128.218:2020': {
                        'NAME': 'OPTIMUS',
                        'ENGINE': 'sql_server.pyodbc',
                        'server': '200.201.128.218,2020',
                        'uid': 'rodolfo.dona',
                        'pwd': 'timeup4t2a3',
                        'OPTIONS':{'driver': 'SQL Server Native Client 11.0'},
                        'vpn':None
                    },
                    '200.201.128.218:1044': {
                        'NAME': 'OPTIMUS',
                        'ENGINE': 'sql_server.pyodbc',
                        'server': '200.201.128.218,1044',
                        'uid': 'ivr_dev',
                        'pwd': 'trestto@123',
                        'OPTIONS':{'driver': 'SQL Server Native Client 11.0'},
                        'vpn':None
                    },
                    '200.206.41.210:21744': {
                        'NAME': 'OPTIMUS',
                        'ENGINE': 'sql_server.pyodbc',
                        'server': '200.206.41.210,21744',
                        'uid': 'ivr_dev',
                        'pwd': 'trestto@123',
                        'OPTIONS':{'driver': 'SQL Server Native Client 11.0'},
                        'vpn':None
                    },
                    '200.206.41.210:22744': {
                        'NAME': 'OPTIMUS',
                        'ENGINE': 'sql_server.pyodbc',
                        'server': '200.206.41.210,22744',
                        'uid': 'ivr_dev',
                        'pwd': 'trestto@123',
                        'OPTIONS':{'driver': 'SQL Server Native Client 11.0'},
                        'vpn':None
                    },
                    '200.170.220.147:10130': {
                        'NAME': 'OPTIMUS',
                        'ENGINE': 'sql_server.pyodbc',
                        'server': '200.170.220.147,10130',
                        'uid': 'ivr_dev',
                        'pwd': 'trestto@123',
                        'OPTIONS':{'driver': 'SQL Server Native Client 11.0'},
                        'vpn':None
                    },
                    '10.100.31.70': {
                        'NAME': 'OPTIMUS',
                        'ENGINE': 'sql_server.pyodbc',
                        'server': '10.100.31.70',
                        'uid': 'satemp',
                        'pwd': 'Intervalor99',
                        'OPTIONS':{'driver': 'SQL Server Native Client 11.0'},
                        'vpn':{'name':'name', 'user':'user','name':'name'}
                    },
                    '200.201.128.218:1818': {
                        'NAME': 'LOG_MONITOR',
                        'ENGINE': 'sql_server.pyodbc',
                        'server': '200.201.128.218,1818',
                        'uid': 'augusto.nascimento',
                        'pwd': 'Sophia@2017',
                        'OPTIONS':{'driver': 'SQL Server Native Client 11.0'},
                        'vpn':{'name':'name', 'user':'user','name':'name'}
                    },
                    '189.112.36.196:3510': {
                        'NAME': 'LOG_MONITOR',
                        'ENGINE': 'sql_server.pyodbc',
                        'server': '189.112.36.196,3510',
                        'uid': 'augusto.nascimento',
                        'pwd': 'Sophia@2017',
                        'OPTIONS':{'driver': 'SQL Server Native Client 11.0'},
                        'vpn':{'name':'name', 'user':'user','name':'name'}
                    },
                }

grammar_servers_list = {
    'developer': {
        '192.168.25.61': {'server_ip':'192.168.25.61', 'user': 'administrator', 'pwd': 'trestto@122', 'type':'unc'},
    },
    'master': {
        '192.168.25.62': {'server_ip':'192.168.25.62', 'user': 'administrator', 'pwd': '!@#rec!@#H7v', 'type':'unc'},
        '200.170.220.147:2101': {'server_ip':'200.170.220.147', 'user': 'recvoz01', 'pwd': '!@#ftp!@#H7v&', 'type':'ftp', 'port':2101},
        '200.170.220.147.2102': {'server_ip':'200.170.220.147', 'user': 'recvoz02', 'pwd': '!@#ftp!@#H7v&', 'type':'ftp', 'port':2102},
        '200.170.220.147.2194': {'server_ip':'200.170.220.147', 'user': 'recvoz94', 'pwd': '!@#ftp!@#H7v&', 'type':'ftp', 'port':2194},
        '200.201.128.218:2191': {'server_ip':'200.201.128.218', 'user': 'recvoz91', 'pwd': '!@#ftp!@#H7v&', 'type':'ftp', 'port':2191},
        '200.201.128.218:2192': {'server_ip':'200.201.128.218', 'user': 'recvoz92', 'pwd': '!@#ftp!@#H7v&', 'type':'ftp', 'port':2192},
        '200.201.128.218:2193': {'server_ip':'200.201.128.218', 'user': 'recvoz93', 'pwd': '!@#ftp!@#H7v', 'type':'ftp', 'port':2193},
    }
}


list_servers_config = [
    {'name':'developer','host':'192.168.25.74','port_number':22,'pwd':'opt!@#H7v','path_audio':'/opt/optimus/locutores','path_ov':'/opt/optimus/ivr'},
    {'name':'homologa_new','host':'192.168.25.93','port_number':22,'pwd':'opt!@#H7v','path_audio':'/opt/optimus/locutores','path_ov':'/opt/optimus/ivr'},
    {'name':'HOMOLOGACAO','host':'192.168.25.58','port_number':22,'pwd':'opt!@#H7v','path_audio':'/opt/optimus/locutores','path_ov':'/opt/optimus/ivr'},
    {'name':'OPTIMUS-INTERVALOR-01','host':'10.100.31.72','port_number':22,'pwd':'opt!@#H7v','path_audio':'/var/lib/asterisk/sounds','path_ov':'/home/optimus/ivr'},
    {'name':'OPTIMUS-INTERVALOR-02','host':'10.100.31.75','port_number':22,'pwd':'opt!@#H7v','path_audio':'/var/lib/asterisk/sounds','path_ov':'/home/optimus/ivr'},
    {'name':'OPTIMUS-CLOUD-01','host':'200.201.128.218','port_number':2261,'pwd':'opt!@#H7v','path_audio':'/var/lib/asterisk/sounds','path_ov':'/home/optimus/ivr'},
    {'name':'OPTIMUS-CLOUD-02','host':'200.201.128.218','port_number':2262,'pwd':'opt!@#H7v','path_audio':'/var/lib/asterisk/sounds','path_ov':'/home/optimus/ivr'},
    # {'name':'OPTIMUS-CLOUD-03','host':'200.201.128.218','port_number':2263,'pwd':'opt!@#H7v','path_audio':'/opt/optimus/locutores','path_ov':'/opt/optimus/ivr'},
    {'name':'OPTIMUS-CLOUD-04','host':'200.201.128.218','port_number':2264,'pwd':'opt!@#H7v','path_audio':'/opt/optimus/locutores','path_ov':'/opt/optimus/ivr'},
    # {'name':'OPTIMUS-CLOUD-05','host':'200.201.128.218','port_number':2265,'pwd':'opt!@#H7v','path_audio':'/opt/optimus/locutores','path_ov':'/opt/optimus/ivr'},
    {'name':'OPTIMUS-CLOUD-06','host':'200.201.128.218','port_number':2266,'pwd':'opt!@#H7v','path_audio':'/opt/optimus/locutores','path_ov':'/opt/optimus/ivr'},
    {'name':'OPTIMUS-CLOUD-07','host':'200.201.128.218','port_number':2267,'pwd':'opt!@#H7v','path_audio':'/opt/optimus/locutores','path_ov':'/opt/optimus/ivr'},
    # {'name':'OPTIMUS-ULTRA-01','host':'192.168.4.11','port_number':22,'pwd':'opt!@#H7v','path_audio':'/var/lib/asterisk/sounds','path_ov':'/home/optimus/ivr'},
    # {'name':'OPTIMUS-ULTRA-02','host':'192.168.4.12','port_number':22,'pwd':'opt!@#H7v','path_audio':'/var/lib/asterisk/sounds','path_ov':'/home/optimus/ivr'},
    # {'name':'OPTIMUS-ULTRA-03','host':'192.168.4.23','port_number':22,'pwd':'opt!@#H7v','path_audio':'/var/lib/asterisk/sounds','path_ov':'/home/optimus/ivr'},
    {'name':'OPTIMUS-ULTRA-04','host':'192.168.4.44','port_number':22,'pwd':'opt!@#H7v','path_audio':'/var/lib/asterisk/sounds','path_ov':'/home/optimus/ivr'},
    {'name':'OPTIMUS-ULTRA-05','host':'192.168.4.45','port_number':22,'pwd':'opt!@#H7v','path_audio':'/var/lib/asterisk/sounds','path_ov':'/home/optimus/ivr'},
    {'name':'OPTIMUS-ULTRA-06','host':'192.168.4.46','port_number':22,'pwd':'opt!@#H7v','path_audio':'/opt/optimus/locutores','path_ov':'/opt/optimus/ivr'},
    {'name':'OPTIMUS-ULTRA-07','host':'192.168.4.47','port_number':22,'pwd':'opt!@#H7v','path_audio':'/var/lib/asterisk/sounds','path_ov':'/home/optimus/ivr'},
    {'name':'OPTIMUS-ULTRA-08','host':'192.168.4.48','port_number':22,'pwd':'opt!@#H7v','path_audio':'/var/lib/asterisk/sounds','path_ov':'/home/optimus/ivr'},
    {'name':'OPTIMUS-ULTRA-09','host':'192.168.4.49','port_number':22,'pwd':'opt!@#H7v','path_audio':'/var/lib/asterisk/sounds','path_ov':'/home/optimus/ivr'},
    {'name':'OPTIMUS-ULTRA-10','host':'192.168.4.43','port_number':22,'pwd':'opt!@#H7v','path_audio':'/var/lib/asterisk/sounds','path_ov':'/home/optimus/ivr'},
    {'name':'OPTIMUS-TELIUM-01','host':'200.170.220.147','port_number':2271,'pwd':'opt!@#H7v','path_audio':'/var/lib/asterisk/sounds','path_ov':'/home/optimus/ivr'},
    {'name':'OPTIMUS-TELIUM-02','host':'200.170.220.147','port_number':2272,'pwd':'opt!@#H7v','path_audio':'/var/lib/asterisk/sounds','path_ov':'/home/optimus/ivr'},
    {'name':'OPTIMUS-TELIUM-03','host':'200.170.220.147','port_number':2273,'pwd':'opt!@#H7v','path_audio':'/var/lib/asterisk/sounds','path_ov':'/home/optimus/ivr'},
    # {'name':'OPTIMUS-TELIUM-05','host':'200.170.220.147','port_number':2275,'pwd':'opt!@#H7v','path_audio':'/var/lib/asterisk/sounds','path_ov':'/home/optimus/ivr'},
    {'name':'OPTIMUS-TELIUM-06','host':'200.170.220.147','port_number':2276,'pwd':'opt!@#H7v','path_audio':'/var/lib/asterisk/sounds','path_ov':'/home/optimus/ivr'},
    # {'name':'OPTIMUS-TELIUM-07','host':'200.170.220.147','port_number':2277,'pwd':'opt!@#H7v','path_audio':'/opt/optimus/locutores','path_ov':'/opt/optimus/ivr'},
    {'name':'OPTIMUS-TELIUM-08','host':'200.170.220.147','port_number':2278,'pwd':'opt!@#H7v','path_audio':'/opt/optimus/locutores','path_ov':'/opt/optimus/ivr'},
    {'name':'OPTIMUS-TELIUM-10','host':'200.170.220.147','port_number':2280,'pwd':'opt!@#H7v','path_audio':'/opt/optimus/locutores','path_ov':'/opt/optimus/ivr'},
    {'name':'TRESTTO-CLOUD-01','host':'200.201.128.218','port_number':2261,'pwd':'opt!@#H7v','path_audio':'/opt/optimus/locutores','path_ov':'/opt/optimus/ivr'},
    {'name':'TRESTTO-CLOUD-02','host':'200.201.128.218','port_number':2262,'pwd':'opt!@#H7v','path_audio':'/opt/optimus/locutores','path_ov':'/opt/optimus/ivr'},
    # {'name':'OPTIMUS-01','host':'192.168.25.51','port_number':22,'pwd':'opt!@#H7v','path_audio':'/opt/optimus/locutores','path_ov':'/opt/optimus/ivr'},
    # {'name':'OPTIMUS-02','host':'192.168.25.52','port_number':22150,'pwd':'opt!@#H7v','path_audio':'/opt/optimus/locutores','path_ov':'/opt/optimus/ivr'},
    {'name':'OPTIMUS-03','host':'192.168.25.53','port_number':22150,'pwd':'opt!@#H7v','path_audio':'/var/lib/asterisk/sounds','path_ov':'/home/optimus/ivr'},
    {'name':'OPTIMUS-04','host':'192.168.25.54','port_number':22,'pwd':'opt!@#H7v','path_audio':'/var/lib/asterisk/sounds','path_ov':'/home/optimus/ivr'},
    {'name':'OPTIMUS-SERVICES-01','host':'189.112.36.196','port_number':2251,'pwd':'opt!@#H7v','path_audio':'/opt/optimus/locutores','path_ov':'/opt/optimus/ivr'}
]
