# -*- coding: iso-8859-1 -*
import os, csv, wave, contextlib, subprocess, time, pyodbc
from shutil import copy2
from pydub import AudioSegment
from settings import list_servers_config, database_list
from instrucoes_asterisk import *

def update_sql_basenomeivr():
      os.system ('cls')
      src = input ("Insira o caminho onde estao os nomes: \n \n")
      #src = origem
      novonome = 0
      nomeexiste = 0
      count = 0
      lista_nomes_inseridos = []
      lista_servidor = {''}

      #print ('O local de origem das gravaoces é:' + origem )
      #print ('\n')


      #src = 'C:/Users/augusto.nascimento/Documents/locutores/pt_BR-lb/nome'
      print ('Lista de Servidores: \n')
      print ('1 -',lista_bd['1'])
      print ('2 -',lista_bd['2'])
      print ('3 -',lista_bd['3'])
      print ('4 -',lista_bd['4'])
      print ('5 -',lista_bd['5'])
      print ('6 -',lista_bd['6'])
      print ('7 -',lista_bd['7'])
      print ('8 -',lista_bd['8'])

      opcao_dst = input ('Insira o numero referente ao banco de dados \n')
      ip_valido = '0'
      while ip_valido == '0':
          if opcao_dst in lista_bd:
              database_dst = lista_bd[''+opcao_dst+'']
              print ('O servidor de destino e: ' + database_dst)
              print ('\n')
              ip_valido = '1'
              break
          else:
              print ('Opcao selecionada fora do range de IPs! \n')
              opcao_dst = input ('Insira o numero referente ao banco de dados \n')
      else:
          print ('Servidor selecionado \n')


      database = database_list[database_dst]

      #database = database_list['189.112.36.196:3510']

      cn = pyodbc.connect(driver='{SQL Server Native Client 11.0}', server=database['server'], uid=database['uid'], pwd=database['pwd'])

      i = []
      for f in os.listdir(src):
          l = f.split("_")
          i = "('{nome}', '{locutor}')".format(nome=l[0], locutor=l[1][:2])
          i2 = "'{nome}', '{locutor}'".format(nome=l[0], locutor=l[1][:2])
          sqlv = "Nome = '{nome}' and Locutor ='{locutor}'".format(nome=l[0], locutor=l[1][:2])
          sqlconsulta = """SELECT * from ovx.[dbo].[BaseNomeIVR] where {condicao}""".format(condicao=sqlv)
          sql = """INSERT INTO ovx.[dbo].[BaseNomeIVR] ([Nome],[Locutor]) VALUES {valor}""".format(valor=i)
          sql2 = """EXEC [ovx].[dbo].[sp_UPDATE_BASE_NOME_IVR] {valor2}""".format(valor2=i2)
          #sql = select * from ovx.[dbo].[BaseNomeIVR] where Nome =
          #print ('O comando sql para consulta se o nome ja esta na base eh: \n' + sqlconsulta)
          #time.sleep(4)
          resultpesquisa = cn.execute(sqlconsulta).fetchone()
          if resultpesquisa:
              #print ('\n Retorno: ' + str(resultpesquisa))
              #print ('\n')
              print ('O nome '+str(l[0])+' no locutor '+str(l[1][:2])+' ja esta na base de dados \n')
              nomeexiste += 1
          else:
              print ('O nome '+str(l[0])+' no locutor '+str(l[1][:2])+' sera inserido na base de dados \n')
              #print ('o comando sql que sera executado eh \n \n' + sql)
              cn.execute(sql2)
              lista_nomes_inseridos.append(sql2)
              lista_nomes_inseridos.append(sql)
              novonome += 1


          #time.sleep(3)



      cn.commit()

      print ('\n Novos nomes inseridos eh ===> ' + str(novonome))
      print ('\n Nomes inseridos: ' ,lista_nomes_inseridos)
      print ('\n Quantidade de nomes nao inseridos pois ja havia na base eh ===> ' + str(nomeexiste))

      arq = open("INSERT_NOMES.txt",'a+')
      for f in lista_nomes_inseridos:
          arq.writelines(lista_nomes_inseridos[count])
          arq.writelines("\n \n")
          count += 1

      arq.close()

      print ('\n Nao ha novos nomes a serem inseridos \n')
      print ('Obrigado e ate breve \n')
      return novonome
