import pyodbc
import pandas as pd
# from ftplib import FTP
import pysftp
from io import StringIO, BytesIO
from settings import database_info


def agente_virtual_info():

    database_list = database_info()

    frames = []
    for database in database_list.values():
        cn = pyodbc.connect(driver='{SQL Server Native Client 11.0}', server=database['server'], uid=database['uid'], pwd=database['pwd'])

        sql = """SELECT cs.Campanha,s.NomeMaquina,s.IP,s.IPExterno,s.PortaSSH, s.IDServidor ,c.ScriptIVR,c.Descricao
                FROM OPTIMUS.dbo.[ScriptServidor] cs
                INNER JOIN OPTIMUS.dbo.[Servidor] s ON cs.IdServidor = s.IDServidor
                INNER JOIN OPTIMUS.dbo.Campanha c ON c.campanha = cs.Campanha
                ORDER BY cs.Campanha
                """
        df = pd.read_sql(sql,cn)
        frames.append(df)

    return  pd.concat(frames)

# cnopts = pysftp.CnOpts()
# cnopts.hostkeys = None


df_agente_virtual = agente_virtual_info()


if __name__ == '__main__':

print(df_agente_virtual.loc[df_agente_virtual['Campanha'] == 3740])
# df_server = df_agente_virtual.groupby(as_index=False, by=['IPExterno', 'PortaSSH']).count()
