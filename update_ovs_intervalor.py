import pyodbc
import pandas as pd
# from ftplib import FTP
import pysftp
from io import StringIO, BytesIO


def database_info ():
    return {
        '10.100.31.70': {
            'NAME': 'OPTIMUS',
            'ENGINE': 'sql_server.pyodbc',
            'server': '10.100.31.70',
            'uid': 'satemp',
            'pwd': 'Intervalor99',
            'OPTIONS':{'driver': 'SQL Server Native Client 11.0'}
            }
        }

def agente_virtual_info():

    database_list = database_info()

    frames = []
    for database in database_list.values():
        cn = pyodbc.connect(driver='{SQL Server Native Client 11.0}', server=database['server'], uid=database['uid'], pwd=database['pwd'])

        sql = """SELECT cs.Campanha,s.NomeMaquina,s.IP,s.IPExterno,s.PortaSSH, s.IDServidor ,c.ScriptIVR,c.Descricao
                FROM OPTIMUS.dbo.[ScriptServidor] cs
                INNER JOIN OPTIMUS.dbo.[Servidor] s ON cs.IdServidor = s.IDServidor
                INNER JOIN OPTIMUS.dbo.Campanha c ON c.campanha = cs.Campanha
                ORDER BY cs.Campanha
                """
        df = pd.read_sql(sql,cn)
        frames.append(df)

    return  pd.concat(frames)

cnopts = pysftp.CnOpts()
cnopts.hostkeys = None


df_agente_virtual = agente_virtual_info()
df_server = df_agente_virtual.groupby(as_index=False, by=['IP']).count()

ftp_user_list = {
    '10.100.31.72': {
        22:{'user':'root', 'pwd':'opt!@#H7v'},
     },
    '10.100.31.75': {
        22:{'user':'root', 'pwd':'opt!@#H7v'},
     },
}

for index, row in df_server.iterrows():

    print(row['IP'])
    if row['IP'] not in ftp_user_list.keys():
        print('ip nao econtrado')
        continue
    with pysftp.Connection(row['IP'], username=ftp_user_list[row['IP']][22]['user'], password=ftp_user_list[row['IP']][22]['pwd'], port=22, cnopts=cnopts) as sftp:
        df_agente = df_agente_virtual.loc[(df_agente_virtual['IP'] == row['IP'])]
        for index2, row2 in df_agente.iterrows():
            path_source = '/home/optimus/ivr/ov/{campanha}/'.format(campanha=str(row2['Campanha'])[:3])
            file_name = "%s.conf" % (row2['ScriptIVR'])
            print(file_name)
            full_name = '{path_source}{file_name}'.format(path_source=path_source, file_name=file_name)
            if sftp.exists(full_name):
                path_destiny = 'C:/Users/augusto.nascimento/Documents/agente_virtual/ivr/ov/{campanha}/{file_name}'.format(campanha=str(row2['Campanha'])[:3], file_name=file_name)
                sftp.get(full_name, path_destiny)
            else:
                path_source = '/home/optimus/ivr/ov/{campanha}0/'.format(campanha=str(row2['Campanha'])[:3])
                full_name = '{path_source}{file_name}'.format(path_source=path_source, file_name=file_name)
                if sftp.exists(full_name):
                    path_destiny = 'C:/Users/augusto.nascimento/Documents/agente_virtual/ivr/ov/{campanha}0/{file_name}'.format(campanha=str(row2['Campanha'])[:3], file_name=file_name)
                    sftp.get(full_name, path_destiny)
            # ftp_download_file(ftp, row2['Campanha'], path, file_name)


# print(df_server)
