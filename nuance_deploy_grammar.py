import git
import time
import os
import csv
import subprocess
from datetime import datetime
import smtplib
from email.mime.text import MIMEText
import shutil

from ftplib import FTP

from settings import EmailUser, grammar_servers_list

class DeployGrammar(object):
	"""docstring for DeployGrammar."""
	def __init__(self):
		super(DeployGrammar, self).__init__()
		self.path_dictionary = {
			'PaschoalottoGlobal': 'BIN_NP_V1',
			'RedeBrasilGlobal': 'BIN_REDE_BRASIL_V1',
			'UltraCenterGlobal': 'BIN_ULTRA_V1',
			'TresttoGlobal': 'BIN_TRESTTO_GLOBAL',
			'MultiCobraGlobal': 'BIN_MULTICOBRA_V1',
			'SystemInteractGlobal': 'BIN_SYSTEM_INTERACT',
			'TresttoTreinamento': 'BIN_TRESTTO_TREINAMENTO',
			'IntervalorGlobal': 'BIN_INTERVALOR_V1',
			'YamahaGlobal': 'BIN_YAMAHA_V1'
		}
	def pull_branch(self, git_branch):
		git_directory = "D:/grammar_{git_branch}".format(git_branch=git_branch)
		g = git.cmd.Git(git_directory)
		print("Checando repositorio {git_branch}: {time}".format(git_branch=git_branch, time=time.ctime()))
		try:
			repo= git.Repo(git_directory)
			o = repo.remotes.origin
			branch = getattr(repo.heads, git_branch)
			refs = getattr(o.refs, git_branch)
			branch.set_tracking_branch(refs)
			branch.checkout()
			repo.git.reset('--hard','origin/{git_branch}'.format(git_branch=git_branch))
			o.pull()
		except:
			print ("Houve erro ao tentar se conectar ao bitbucket : %s" % time.ctime())

	def files_changes_check(self, git_branch):
		directory = 'D:/grammar_{git_branch}'.format(git_branch=git_branch)

		file_string = 'controle_alteracoes_{git_branch}.csv'.format(git_branch=git_branch)

		with open(file_string, 'r', newline='') as csvfile:
			spamreader = csv.reader(csvfile, delimiter=',', quotechar='"')
			file_list = []
			for row in spamreader:
				file_list.append(row)

		file_changed = []
		file_list_new = []
		for root, directories, files in os.walk(directory):
			directories[:] = [d for d in directories if d not in ['.git', 'cpqd', 'global']]
			for f in files:
				file_name = os.path.join(os.path.abspath(root), f)
				v = True
				for row in file_list:
					if file_name == row[0]:
						if os.path.getmtime(file_name) <= float(row[1]):
							v = False
						break
				if v:
					if file_name.split('.')[-1]=='grxml' and file_name[:3].lower()!='sub' and file_name[:4].lower()!='cpqd':
						file_changed.append([file_name, git_branch, 1, 1])
				file_list_new.append([file_name, os.path.getmtime(file_name)])
		return file_changed, file_list_new

	def grammar_compile(self, file_path):
		file_name = file_path.split('\\')[-1]
		command = 'sgc %s' %(file_path)
		x = 1
		if file_path.split('\\')[-2] in list(self.path_dictionary.keys()) and file_name[:3].lower()!='sub':
			project = file_path.split('\\')[-2]
			x = subprocess.call(command)
		else:
			project = file_path.split('\\')[-2]
			try:
				project_number = int(project[:3])
			except:
				project_number = None
			if project_number and file_name[:3].lower()!='sub':
				x = subprocess.call(command)
		return x

	def upload_file(self, file_changed, server, server_config):
		if not file_changed:
			return
		if server_config['type'] == 'unc':

			if not os.path.exists("//{server_ip}/c$/".format(server_ip=server_config['server_ip'])):
				winCMD = 'NET USE \\\\' + server_config['server_ip'] + '\\c$ /User:' + server_config['user'] + ' ' + server_config['pwd']
				subprocess.Popen(winCMD, stdout=subprocess.PIPE, shell=True)

			for file_properties in file_changed:
				file_path = file_properties[0]
				file_name = file_path.split('\\')[-1]
				if file_properties[2] == 0:
					if file_path.split('\\')[-2] in list(self.path_dictionary.keys()) and file_name[:3].lower()!='sub':
						project = file_properties[0].split('\\')[-2]
						path_base = '//{server_ip}/c$/ProgramData/Nuance/Enterprise/Nuance/Grammars/{path_destiny}'.format(server_ip=server_config['server_ip'],path_destiny=self.path_dictionary[project])
					else:
						project = file_path.split('\\')[-2]
						try:
							project_number = int(project[:3])
						except:
							project_number = None
						if project_number and file_name[:3].lower()!='sub':
							path_base = '//{server_ip}/c$/ProgramData/Nuance/Enterprise/Nuance/Grammars/BIN_{project_number}_V1'.format(server_ip=server_config['server_ip'],project_number=project_number)

					if not os.path.exists(path_base):
						os.mkdir(path_base)

					file_name = file_name.replace('.grxml', '.gram')
					file_destiny = '{path_base}/{file_name}'.format(path_base=path_base, file_name=file_name)
					shutil.copyfile(os.path.join(os.getcwd(),file_name),file_destiny)

		if server_config['type'] == 'ftp':
			print(server_config['server_ip'], server_config['type'])
			ftp = FTP()
			ftp.connect(server_config['server_ip'], server_config['port'])
			ftp.login(server_config['user'], server_config['pwd'])

			for file_properties in file_changed:
				file_path = file_properties[0]
				file_name = file_path.split('\\')[-1]
				path_destiny = None
				ftp.cwd('/grammar')
				if file_path.split('\\')[-2] in list(self.path_dictionary.keys()) and file_name[:3].lower()!='sub':
					project = file_properties[0].split('\\')[-2]
					path_destiny = self.path_dictionary[project]
				else:
					project = file_path.split('\\')[-2]
					try:
						project_number = int(project[:3])
					except:
						project_number = None
					if project_number and file_name[:3].lower()!='sub':
						if project_number = 306:
							path_destiny = 'BIN_YAMAHA_{project_number}_V1'.format(project_number=project_number)
						else:
							path_destiny = 'BIN_{project_number}_V1'.format(project_number=project_number)

				if path_destiny:
					if path_destiny not in ftp.nlst():
						ftp.mkd(path_destiny)
					ftp.cwd(path_destiny)
					file_name = file_name.replace('.grxml', '.gram')
					f = open(file_name, 'rb')
					ftp.storbinary('STOR %s' % file_name, f)
					f.close()

def send_email(file_name):

    msg = MIMEText("Ops... Algo n�o saiu conforme o esperado \n Houve um erro durante a compila��o do arquivo {file_name}. Verifique".format(file_name=file_name))

    msg['Subject'] = "[ERRO COMPILA��O] [{file_name}]".format(file_name=file_name)

    #put your host and port here
    s = smtplib.SMTP_SSL('email-ssl.com.br:465')
    s.login(EmailUser.user,EmailUser.password)
    s.sendmail('trestto.agente.virtual@trestto.com.br', msg.as_string())
    s.quit()

git_branch_list = ['developer', 'master']

while 1:
	for git_branch in git_branch_list:
		Grammar = DeployGrammar()
		Grammar.pull_branch(git_branch)
		file_changed, file_list_new = Grammar.files_changes_check(git_branch)

		file_changed = [[row[0], row[1], Grammar.grammar_compile(row[0]), row[3]] for row in file_changed]

		for server, server_config in  grammar_servers_list[git_branch].items():
			Grammar.upload_file(file_changed, server, server_config)

		csv_string = 'controle_alteracoes_{git_branch}.csv'.format(git_branch=git_branch)
		with open(csv_string, 'w', newline='') as csvfile:
			spamwriter = csv.writer(csvfile, delimiter=',',
						quotechar='"', quoting=csv.QUOTE_MINIMAL)
			for f in file_list_new:
				if f[0] in [x[0].split('\\')[-1] for x in file_changed]:
					if f[0] in [x[0].split('\\')[-1] for x in file_changed if x[2]==0]:
						spamwriter.writerow(f)
				else:
					spamwriter.writerow(f)
		#
		for f in file_changed:
			if f[2] == 0:
				file_name = f[0].split('\\')[-1]
				file_name = file_name.replace('.grxml', '.gram')
				os.remove(file_name)
	time.sleep(15)
