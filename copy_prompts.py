# -*- coding: iso-8859-1 -*
import os, csv, wave, contextlib
from shutil import copy2

def ordem_numero(x,n=0):
    if x >= 10**n:
        return ordem_numero(x,n+1)
    else:
        return n

def path_valor(x,n):
    if n <= 2:
        return '1_99'
    elif n <= 3:
        return '{f}{ordem}_{f}{ordem9}'.format(f=str(x)[0:1],ordem='0'*(n-1),ordem9='9'*(n-1))
    elif n <= 5:
        return '1000_99000'

paths = [
        ['T:/Operacional/Base Audios/Geraldo Cortes/Reais/Opera��o/Tratados', 'gc'],
    ]

# valores_lista = [i for i in range(1,1000)]
# valores_lista = valores_lista + [i for i in range(1000,99001,1000)]
# for i in valores_lista:
#     print(i, ordem_numero(i), path_valor(i, ordem_numero(i)))



# csvfile = open('prompts_faltando.csv', 'w', newline='')
# spamwriter = csv.writer(csvfile, delimiter=',',
#                  quotechar='"', quoting=csv.QUOTE_MINIMAL)


for locutor in paths:
    print(locutor[0])
    directory_src = locutor[0]
    directory_dst = 'C:/Users/augusto.nascimento/Documents/locutores/pt_BR-{locutor}/vocaliza_valor/700_799'.format(locutor=locutor[1])


    #
    # prompts_lista = [[path_valor(i, ordem_numero(i)),'{i}_reais_op1_{locutor}.wav'.format(i=str(i).zfill(2), locutor=locutor[1])]  for i in valores_lista]
    # prompts_lista = prompts_lista + [['1_99', '{i}_real_op1_{locutor}.wav'.format(i=str(1).zfill(2), locutor=locutor[1])]]
    #
    # for p in prompts_lista:
    #     src = os.path.join(directory_src,p[1])
    #     if not os.path.isfile(src):
    #         pass
    #         # log_text = [directory_src, p[1]]
    #         # spamwriter.writerow(log_text)
    #     else:
    #         if not os.path.isdir(os.path.join(directory_dst, p[0])):
    #             os.makedirs(os.path.join(directory_dst, p[0]))
    #         dst = os.path.join(directory_dst, p[0],p[1])
    #         print(dst)
    #         with contextlib.closing(wave.open(src,'r')) as f_prompt:
    #             if f_prompt.getframerate() == 8000 and f_prompt.getnchannels() == 1:
    #                 copy2(src, dst)
    #             else:
    #                 print("arquivo fora de formato:", locutor[1], src)
    #
    #         # continue
        # print(p)

# csvfile.close()
    for f in os.listdir(locutor[0]):
        src = os.path.join(directory_src, f)
        if not os.path.isdir(directory_dst):
            os.makedirs(directory_dst)
        dst = os.path.join(directory_dst, f)
        print(f)
        if f.split(".")[-1] == 'wav':
            with contextlib.closing(wave.open(src,'r')) as f_prompt:
                if f_prompt.getframerate() == 8000 and f_prompt.getnchannels() == 1:
                    copy2(src, dst)
                else:
                    print("arquivo fora de formato:", locutor[1], src)
