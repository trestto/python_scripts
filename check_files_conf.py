import os
import sys
import csv

def find_variable_value(row_string, variable):
    if row_string.find(variable)>1:
        row_string = row_string[row_string.find(variable):]
        row_string = row_string.split(',')[0]
        if not row_string.find("=")>0:
            return None

        row_string = row_string.split('=')[1]
        row_string = ''.join([s for s in row_string if s not in ['(', '\n', '\t', ',', ')', ' ', '$','{', '}']])
        variable = ''.join([s for s in variable if s not in ['(', '\n', '\t', ',', ')', ' ', '$','{', '}']])
        if row_string.find(variable)>0:
            return None
        elif not row_string:
            None
        else:
            return row_string
    else:
        return None

csvfile = open('eggs.csv', 'w', newline='')
spamwriter = csv.writer(csvfile, delimiter=',',
                quotechar='"', quoting=csv.QUOTE_MINIMAL)
spamwriter.writerow(['projeto', 'versao', 'status_ura', 'sub_status_ura'])

variable = ['(STATUS', 'SUB_STATUS']
for root, dirs, files in os.walk('C:/Users/augusto.nascimento/Documents/ov'):
    for name in files:
        if name.split(".")[-1] == 'conf':
            csv_list = []
            with open(os.path.join(root, name), 'r', errors='ignore') as file:
                for row in file:
                    csv_list.append(row)
            for i in range(len(csv_list)-1):
                if len(csv_list[i])>1:
                    variable_return_list = [None for i in range(2)]
                    if csv_list[i].find(variable[0])>0 and csv_list[i].find(variable[1])>0:
                        variable_return_list[0] = find_variable_value(csv_list[i], variable[0])
                        variable_return_list[1] = find_variable_value(csv_list[i], variable[1])
                    elif csv_list[i].find(variable[0])>0 and csv_list[i + 1].find(variable[0]):
                        variable_return_list[0] = find_variable_value(csv_list[i], variable[0])
                        variable_return_list[1] = find_variable_value(csv_list[i+1], variable[1])
                        i+=1
                    if not all(v is None for v in variable_return_list):
                        name = name.split(".")[0]
                        if len(name.split("_"))>=4:
                            spamwriter.writerow([name.split("_")[1], name.split("_")[3], variable_return_list[0], variable_return_list[1]])
                        else:
                            spamwriter.writerow([name.split("_")[1], None, variable_return_list[0], variable_return_list[1]])
csvfile.close()
