import time
import os
import csv
import subprocess
import datetime

def files_changes_check():

    directory = 'C:/Users/augusto.nascimento/Documents/Grammar'

    with open('controle_alteracoes_new.csv', 'w', newline='') as csvfile_new:
        spamwriter = csv.writer(csvfile_new, delimiter=',',
                        quotechar='"', quoting=csv.QUOTE_MINIMAL)

        with open('controle_alteracoes.csv', newline='') as csvfile:
            spamreader = csv.reader(csvfile, delimiter=',', quotechar='"')
            list_files = []
            for row in spamreader:
                list_files.append(row)

        for root, directories, files in os.walk(directory):
            directories[:] = [d for d in directories if d not in ['.git']]
            for f in files:
                file_name = os.path.join(os.path.abspath(root), f)
                v = True
                for row in list_files:
                    if file_name == row[0]:
                        if os.path.getmtime(file_name) <= float(row[1]):
                            v = False
                        break
                if v:
                    if file_name.split('.')[-1]=='grxml':
                        grammar_compile(file_name)
                spamwriter.writerow([file_name, os.path.getmtime(file_name)])

                # print(filePath)
    os.remove('controle_alteracoes.csv')
    # #Move new file
    os.rename('controle_alteracoes_new.csv', 'controle_alteracoes.csv')

def grammar_compile(file_name):
    command = 'sgc %s' %(file_name)
    x = subprocess.check_output('sgc C:/Users/Administrator/Documents/Nuance/PaschoalottoGlobal/Fontes/NP_COLETAR_RECEBIMENTO_BOLETO_TIPO_1_V1.grxml')
    with open('log_compile.csv', newline='') as csvfile:
        spamwriter = csv.writer(csvfile, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
        spamwriter.writerow([datetime.now(), file_name, x])

files_changes_check()
