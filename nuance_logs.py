import os
import re
from collections import OrderedDict
import json
import subprocess
import pyodbc
from datetime import datetime
from ftplib import FTP
from io import StringIO, BytesIO
from settings import database_list, grammar_servers_list


def remove_duplicates(my_list):
    return_list = []
    for item in my_list:
        if item not in return_list:
            return_list.append(item)
    return return_list

def return_unc_file(server_ip, user, pwd):

    path = '//{server_ip}/c$/ProgramData/Nuance/Enterprise/system/diagnosticLogs'.format(server_ip=server_ip)
    try:
        f = open(os.path.join(path, 'nrs.log'), 'r', newline='')
        f.close()
    except:
        winCMD = 'NET USE \\\\' + server_ip + '\\c$ /User:' + user + ' ' + pwd
        subprocess.Popen(winCMD, stdout=subprocess.PIPE, shell=True)

    error_list = []
    with open(os.path.join(path, 'nrs.log'), 'r', newline='') as log_file:
        data = log_file.readlines()

    return data

def return_ftp_file(server_ip, user, pwd, port):
    print(server_ip, user, pwd)
    # cnopts = pysftp.CnOpts()
    # cnopts.hostkeys = None
    ftp = FTP()
    ftp.connect(server_ip, port)
    ftp.login(user, pwd)
    r = BytesIO()
    ftp.retrbinary('RETR /logs/nrs.log',  r.write)

    data = r.getvalue().decode()
    data = data.split('\n')
    print("quantidade de linhas", len(data))
    return data

database = database_list['200.201.128.218:1818']

# cn = pyodbc.connect(driver='{SQL Server Native Client 11.0}', server=database['server'], uid=database['uid'], pwd=database['pwd'])
#
# sql = """CREATE TABLE #nuance_log_error_temp (
#         	server_ip varchar(20),
#         	error_type varchar(50),
#         	grammar_file varchar(max),
#         	event_datetime datetime2,
#             server_port varchar(10),
#         );"""
#
# cn.execute(sql)

list_servers = {values for key, values in grammar_servers_list.items()}
print(list_servers)

# list_servers = {
#     '192.168.25.60': {'server_ip':'192.168.25.60', 'user': 'administrator', 'pwd': 'trestto@122', 'type':'unc', 'server_port':None},
#     '192.168.25.61': {'server_ip':'192.168.25.61', 'user': 'administrador', 'pwd': '!@#rec!@#H7v', 'type':'unc', 'server_port':None},
#     '192.168.25.62': {'server_ip':'192.168.25.62', 'user': 'administrator', 'pwd': '!@#rec!@#H7v', 'type':'unc', 'server_port':None},
#     '200.170.220.147.1': {'server_ip':'200.170.220.147', 'user': 'recvoz01', 'pwd': '!@#ftp!@#H7v&', 'type':'ftp', 'server_port':2101},
#     '200.170.220.147.2': {'server_ip':'200.170.220.147', 'user': 'recvoz02', 'pwd': '!@#ftp!@#H7v&', 'type':'ftp', 'server_port':2102},
#     '200.170.220.147.3': {'server_ip':'200.170.220.147', 'user': 'recvoz94', 'pwd': '!@#ftp!@#H7v&', 'type':'ftp', 'server_port':2194},
#     '200.201.128.218.1': {'server_ip':'200.201.128.218', 'user': 'recvoz91', 'pwd': '!@#ftp!@#H7v&', 'type':'ftp', 'server_port':2191},
#     '200.201.128.218.2': {'server_ip':'200.201.128.218', 'user': 'recvoz92', 'pwd': '!@#ftp!@#H7v&', 'type':'ftp', 'server_port':2192},
#     '200.201.128.218.3': {'server_ip':'200.201.128.218', 'user': 'recvoz93', 'pwd': '!@#ftp!@#H7v', 'type':'ftp', 'server_port':2193},
#     }
#
# log_return = OrderedDict()
# for server, config in list_servers.items():
#     server_ip = config['server_ip']
#     if config['type'] == 'unc':
#         data = return_unc_file(server_ip, config['user'], config['pwd'])
#     elif config['type'] == 'ftp':
#         data = return_ftp_file(server_ip, config['user'], config['pwd'], config['server_port'])
#     else:
#         continue
#
#     error_list = []
#     values = []
#     for row in data:
#         if row:
#             log_datetime = datetime.strptime(row.split("|")[0], '%Y%m%d%H%M%S%f')
#             if re.search('.gram not found.', row):
#                 msg = re.search('uri(.*)not found.', row).group(1)
#                 values.append("""('{server_ip}', 'file_not_found', '{msg}', '{log_datetime}', '{server_port}')""".format(server_ip=server_ip, msg=msg,log_datetime=log_datetime,server_port=config['server_port']))
#                 # values.append("""('{server_ip}', 'no_valid_license_allocated', '{msg}', '{log_datetime}')""".format(server_ip=server_ip, msg='indeterminada',log_datetime=log_datetime))
#             if row.find('SWIrecRecognizerCreate | Licensing')>1:
#                 values.append("""('{server_ip}', 'no_valid_license_allocated', null, '{log_datetime}', '{server_port}')""".format(server_ip=server_ip, log_datetime=log_datetime,server_port=config['server_port']))
#                 # print(row)
#
#     if values:
#         sql = """INSERT INTO #nuance_log_error_temp (server_ip,error_type,grammar_file,event_datetime,server_port) values {values}""".format(values=",".join(values))
#         cn.execute(sql)
#
#         sql = """INSERT INTO nuance_log_error (server_ip,error_type,grammar_file,event_datetime,server_port)
#                 SELECT server_ip,error_type,grammar_file,event_datetime,server_port
#                 FROM #nuance_log_error_temp temp
#                 WHERE NOT EXISTS (
#                 	SELECT 1
#                 	FROM nuance_log_error l
#                 	WHERE
#                 		l.server_ip = temp.server_ip
#                 		and l.error_type = temp.error_type
#                 		and l.grammar_file = temp.grammar_file
#                 		and l.event_datetime = temp.event_datetime
#                         and l.server_port = temp.server_port
#                 	)"""
#
#         cn.execute(sql)
#     cn.commit()
#     if error_list:
#         log_return[server_ip] = remove_duplicates(error_list)
# # print(json.dumps(log_return, indent=4, sort_keys=True))
