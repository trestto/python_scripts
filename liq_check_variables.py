import pyodbc
import pandas as pd
# from ftplib import FTP
import pysftp
from io import StringIO, BytesIO
from settings import database_info


class LiqVariables(object):
    """docstring for LiqVariables."""
    def __init__(self, speaker_initials, project_number):
        super(LiqVariables).__init__()
        self.speaker_initials = speaker_initials
        self.project_number = project_number
        database = database_info()['200.201.128.218:1044']
        self.cn = pyodbc.connect(driver='{SQL Server Native Client 11.0}', server=database['server'], uid=database['uid'], pwd=database['pwd'])

    def minute(self):
        sql = """SELECT optimus.dbo.FREMOVE_ACENTOS(UPPER(REPLACE(REPLACE(REPLACE(ATUAL_MINUTOS, CHAR(160), ''), ' ', '_'), '.', ''))) as variables
                    FROM optimus.dbo.[{project_number}_Dadoscliente]
                    WHERE idlista= 5080005
                    GROUP BY optimus.dbo.FREMOVE_ACENTOS(UPPER(REPLACE(REPLACE(REPLACE(ATUAL_MINUTOS, CHAR(160), ''), ' ', '_'), '.', '')))
                    UNION ALL
                SELECT optimus.dbo.FREMOVE_ACENTOS(UPPER(REPLACE(REPLACE(REPLACE(OFERTA_MINUTOS, CHAR(160), ''), ' ', '_'), '.', ''))) as variables
                    FROM optimus.dbo.[{project_number}_Dadoscliente]
                    WHERE idlista= 5080005
                    GROUP BY optimus.dbo.FREMOVE_ACENTOS(UPPER(REPLACE(REPLACE(REPLACE(OFERTA_MINUTOS, CHAR(160), ''), ' ', '_'), '.', '')))
                    """.format(project_number=self.project_number)
        return  pd.read_sql(sql,self.cn)

    def data(self):
        sql = """SELECT optimus.dbo.FREMOVE_ACENTOS(UPPER(REPLACE(REPLACE(REPLACE(ATUAL_DADOS, CHAR(160), ''), ' ', '_'), '.', ''))) as variables
                    FROM optimus.dbo.[{project_number}_Dadoscliente]
                    WHERE idlista= 5080005
                    GROUP BY optimus.dbo.FREMOVE_ACENTOS(UPPER(REPLACE(REPLACE(REPLACE(ATUAL_DADOS, CHAR(160), ''), ' ', '_'), '.', '')))
                    UNION ALL
                SELECT optimus.dbo.FREMOVE_ACENTOS(UPPER(REPLACE(REPLACE(REPLACE(OFERTA_DADOS, CHAR(160), ''), ' ', '_'), '.', ''))) as variables
                    FROM optimus.dbo.[{project_number}_Dadoscliente]
                    WHERE idlista= 5080005
                    GROUP BY optimus.dbo.FREMOVE_ACENTOS(UPPER(REPLACE(REPLACE(REPLACE(OFERTA_DADOS, CHAR(160), ''), ' ', '_'), '.', '')))
                    """.format(project_number=self.project_number)
        return  pd.read_sql(sql,self.cn)

    def description(self):
        sql = """SELECT optimus.dbo.FREMOVE_ACENTOS(UPPER(REPLACE(REPLACE(NOME_OFERTA, CHAR(160), ''), ' ', '_'))) variables
                FROM optimus.dbo.[{project_number}_Dadoscliente]
                WHERE idlista= 5080005
                GROUP BY optimus.dbo.FREMOVE_ACENTOS(UPPER(REPLACE(REPLACE(NOME_OFERTA, CHAR(160), ''), ' ', '_')))
                    """.format(project_number=self.project_number)
        return  pd.read_sql(sql,self.cn)

    def prompt_record(self):
        cnopts = pysftp.CnOpts()
        cnopts.hostkeys = None
        with pysftp.Connection('192.168.25.74', username='root', password='opt!@#H7v', cnopts=cnopts) as sftp:
            with sftp.cd('/opt/optimus/locutores/pt_BR-{speaker_initials}/plano_celular/'.format(speaker_initials=self.speaker_initials)):
                prompts = sftp.listdir()
            with sftp.cd('/opt/optimus/locutores/pt_BR-{speaker_initials}/plano_celular_descricao/'.format(speaker_initials=self.speaker_initials)):
                description = sftp.listdir()

        return prompts, description

LIQ = LiqVariables(speaker_initials='gs',project_number=508)
prompts, description = LIQ.prompt_record()

variables = LIQ.minute()
variables['variables'] = variables['variables'] + "_{speaker_initials}.wav".format(speaker_initials='gs')
variables['prompt'] = variables['variables'].isin(prompts)
print(variables.loc[variables['prompt'] == False])

variables = LIQ.data()
variables['variables'] = variables['variables'] + "_{speaker_initials}.wav".format(speaker_initials='gs')
variables['prompt'] = variables['variables'].isin(prompts)
print(variables.loc[variables['prompt'] == False])

variables = LIQ.description()
variables['variables'] = variables['variables'] + ".wav".format(speaker_initials='gs')
variables['prompt'] = variables['variables'].isin(description)
vaiable_filter = variables.loc[variables['prompt'] == False]
with pd.option_context('display.max_rows', None, 'display.max_columns', 3, 'display.max_colwidth',150):
    print(vaiable_filter)
