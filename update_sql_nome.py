# -*- coding: iso-8859-1 -*
import os, csv, wave, contextlib
from shutil import copy2
from pydub import AudioSegment
import pyodbc

from settings import list_servers_config, database_list

src = 'C:/Users/augusto.nascimento/Documents/locutores/pt_BR-lb/nome'

database = database_list['189.112.36.196:3510']

cn = pyodbc.connect(driver='{SQL Server Native Client 11.0}', server=database['server'], uid=database['uid'], pwd=database['pwd'])

i = []
for f in os.listdir(src):
    l = f.split("_")
    i = "('{nome}', '{locutor}')".format(nome=l[0], locutor=l[1][:2])
    sql = """INSERT INTO ovx.[dbo].[BaseNomeIVR] ([Nome],[Locutor]) VALUES {valor}""".format(valor=i)

    cn.execute(sql)

cn.commit()
