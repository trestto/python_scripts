# -*- coding: iso-8859-1 -*
import sys, paramiko, csv, os, pysftp, threading
from settings import list_servers_config
from datetime import datetime



class ConvertFormatos (threading.Thread):
   def __init__(self, thread_id, server,command_list):
      threading.Thread.__init__(self)
      self.thread_id = thread_id
      self.server = server
      self.command_list = command_list
   def run(self):
      print ("Starting " + self.server['name'])
      convert_formatos(self.server, self.command_list)
      print ("Exiting " + self.server['name'])


def convert_formatos(server,command_list):
    client = paramiko.SSHClient()
    client.load_system_host_keys()
    client.set_missing_host_key_policy(paramiko.WarningPolicy)
    client.connect(server['host'], port=server['port_number'], username='root',password=server['pwd'])

    command = 'chown -R asterisk {path_audio}'.format(path_audio=server['path_audio'])
    stdin, stdout, stderr = client.exec_command(command)
    print(server['name'], command, stdout.channel.recv_exit_status())
    command = 'chmod -R 775 {path_audio}'.format(path_audio=server['path_audio'])
    stdin, stdout, stderr = client.exec_command(command)
    print(server['name'], command, stdout.channel.recv_exit_status())

    cont_success, cont_error = 0,0
    for command in command_list:
        stdin, stdout, stderr = client.exec_command(command)
        msg = "".join(stdout.readlines())
        if not stdout.channel.recv_exit_status() == 0:
            cont_error = cont_error + 1
        elif msg.find("failed") >= 0:
            cont_error = cont_error + 1
        else:
            cont_success = cont_success + 1
    client.close()

    csvfile = open('log_upload_prompts.csv', 'a', newline='')
    spamwriter = csv.writer(csvfile, delimiter=',',
                     quotechar='"', quoting=csv.QUOTE_MINIMAL)
    log_text = [datetime.now(), server['name'], server['host'],'file conversion', None, cont_success, cont_error, None]
    spamwriter.writerow(log_text)

    print("servidor {server_name} -> {cont_success} arquivos convertidos - {cont_error} erros ".format(server_name=server['name'], cont_success=cont_success, cont_error=cont_error))

def func_command_list(path_audio, locutor, variable, file_name):
    command_list = []
    for formato_saida in ['gsm','g729','al','alaw','sln']:
        file_destiny = file_name.replace(".wav", "." + formato_saida)
        command_list.append(
                """asterisk -rx 'file convert {path_audio}/pt_BR-{locutor}/{variable}/{file_name} {path_audio}/pt_BR-{locutor}/{variable}/{file_destiny}'"""
                .format(
                    path_audio=path_audio,
                    locutor=locutor,
                    variable=variable,
                    file_name=file_name,
                    file_destiny=file_destiny)
                    )
    return command_list

def prompts_upload(variable, list_to_update):
    threads = []
    thread_id = 0
    for server in list_to_update:
        command_list = []
        print(server)
        with pysftp.Connection(server['host'], username='root', password=server['pwd'], port=server['port_number'], cnopts=cnopts) as sftp:
            for locutor in ['gc','gs','jm','lb','mc','sf','ap','br','fa','ga']:
                contagem = 0
                src = 'C:/Users/augusto.nascimento/Documents/locutores/pt_BR-{locutor}/{variable}'.format(locutor=locutor, variable=variable)
                dst = '{path_audio}/pt_BR-{locutor}/{variable}'.format(path_audio=server['path_audio'], locutor=locutor, variable=variable)
                print(server['name'], dst)
                if os.path.isdir(src):
                    if not sftp.isdir(dst):
                        sftp.makedirs(dst)
                    with sftp.cd(dst):
                        for f in os.listdir(src):
                            if sftp.isfile(f):
                                if int(os.path.getmtime(os.path.join(src,f))) != int(sftp.stat(f).st_mtime):
                                    contagem = contagem + 1
                                    sftp.put(os.path.join(src,f), preserve_mtime=True)
                                    command_list = command_list + func_command_list(server['path_audio'], locutor, variable, f)
                            else:
                                contagem = contagem + 1
                                sftp.put(os.path.join(src,f), preserve_mtime=True)
                                command_list = command_list + func_command_list(server['path_audio'], locutor, variable, f)
                            # sys.stdout.write('.')
                            # sys.stdout.flush()
                        print("")

                        csvfile = open('log_upload_prompts.csv', 'a', newline='')
                        spamwriter = csv.writer(csvfile, delimiter=',',
                                         quotechar='"', quoting=csv.QUOTE_MINIMAL)
                        log_text = [datetime.now(), server['name'], server['host'],'file update', contagem, None, None, None]
                        spamwriter.writerow(log_text)

                        print('carregados {contagem} arquivos para o servidor {server_name} locutor {locutor}'.format(contagem=contagem,server_name=server['name'],locutor=locutor))
                break
        server_updated.append(server['name'])
        t = ConvertFormatos(thread_id=thread_id+1,server=server,command_list=command_list)
        t.start()
        threads.append(t)

    print('prompts carregados para', server_updated)
    # Wait for all of them to finish
    for x in threads:
        x.join()




cnopts = pysftp.CnOpts()
cnopts.hostkeys = None

server_updated = []
server_not_updated = []

list_developer = ['developer']

#CLOUD
list_cloud = ['OPTIMUS-CLOUD-01','OPTIMUS-CLOUD-02','OPTIMUS-CLOUD-03','OPTIMUS-CLOUD-04','OPTIMUS-CLOUD-05','OPTIMUS-CLOUD-06']
#TELIUM
list_telium = ['OPTIMUS-TELIUM-01','OPTIMUS-TELIUM-02','OPTIMUS-TELIUM-03','OPTIMUS-TELIUM-04','OPTIMUS-TELIUM-05','OPTIMUS-TELIUM-06','OPTIMUS-TELIUM-07','OPTIMUS-TELIUM-08','OPTIMUS-TELIUM-10']
#TRESTTO
list_trestto = ['TRESTTO-OPTIMUS-03', 'TRESTTO-OPTIMUS-04']

#INTERVALOR
list_intervalor = ['OPTIMUS-INTERVALOR-01','OPTIMUS-INTERVALOR-02']
#ULTRA
list_ultracenter = ['OPTIMUS-ULTRA-01','OPTIMUS-ULTRA-02','OPTIMUS-ULTRA-03','OPTIMUS-ULTRA-04','OPTIMUS-ULTRA-05','OPTIMUS-ULTRA-06','OPTIMUS-ULTRA-07','OPTIMUS-ULTRA-08','OPTIMUS-ULTRA-09','OPTIMUS-ULTRA-10']

#
list_to_update = [x for x in list_servers_config if x['name'] in list_trestto]
# list_to_update = list_to_update + [x for x in list_servers_config if x['name'] in list_telium]
# list_to_update = list_to_update + [x for x in list_servers_config if x['name'] in list_trestto]
# list_to_update = [x for x in list_servers_config if x['name'] in list_developer]


variable_list = [
    'vocalizar_valor/1_99',
    'vocalizar_valor/100_199',
    'vocalizar_valor/200_299',
    'vocalizar_valor/300_399',
    'vocalizar_valor/400_499',
    'vocalizar_valor/500_599',
    'vocalizar_valor/600_699',
    'vocalizar_valor/700_799',
    'vocalizar_valor/800_899',
    'vocalizar_valor/900_999',
    'vocalizar_valor/1000_99000',
]
for variable in variable_list:
    prompts_upload(variable, list_to_update)

print('convers�o finalizada')
