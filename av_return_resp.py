import pyodbc
import pandas as pd
# from ftplib import FTP
import pysftp
import re
from io import StringIO, BytesIO
from settings import database_list


def agente_virtual_info():

    database = database_list['200.201.128.218:2020']

    frames = []
    cn = pyodbc.connect(driver='{SQL Server Native Client 11.0}',
                        server=database['server'],
                        uid=database['uid'],
                        pwd=database['pwd']
                        )

    sql = """EXEC [sp_tools_campanha_servidor]"""

    df = pd.read_sql(sql,cn)
    frames.append(df)

    return  pd.concat(frames)

def agente_virtual_info_intervalor():

    database = database_list['10.100.31.70']

    frames = []
    cn = pyodbc.connect(driver='{SQL Server Native Client 11.0}',
                        server=database['server'],
                        uid=database['uid'],
                        pwd=database['pwd']
                        )

    sql = """SELECT
        		'TELIUM_BD_10' NomeBD,
        		s.NomeMaquina COLLATE Latin1_General_CI_AS NomeMaquina,
        		ss.Campanha,
        		s.IP COLLATE Latin1_General_CI_AS IP,
        		s.IPExterno COLLATE Latin1_General_CI_AS IPExterno,
        		s.PortaSSH COLLATE Latin1_General_CI_AS PortaSSH,
        		s.IDServidor,
        		c.ScriptIVR COLLATE Latin1_General_CI_AS ScriptIVR,
        		c.Descricao COLLATE Latin1_General_CI_AS Descricao
        	from OPTIMUS.dbo.[scriptServidor] ss
        	INNER JOIN OPTIMUS.dbo.[Servidor] s ON ss.IdServidor = s.IDServidor
        	INNER JOIN OPTIMUS.dbo.Campanha c ON c.campanha = ss.Campanha
        	WHERE c.Ativa =1"""

    df = pd.read_sql(sql,cn)
    frames.append(df)

    return  pd.concat(frames)

# cnopts = pysftp.CnOpts()
# cnopts.hostkeys = None



table = []
if __name__ == '__main__':
    df_agente_virtual = agente_virtual_info_intervalor()
    for index, row in df_agente_virtual.iterrows():
        project_number = row['ScriptIVR'].split('_')[1]
        print(row['ScriptIVR'])
        if project_number.isdigit() and len(project_number) == 3:
            with open('C:/Users/augusto.nascimento/Documents/agente_virtual/ivr/ov/{project_number}/{ScriptIVR}.conf'.format(project_number=project_number,ScriptIVR=row['ScriptIVR']), 'r', newline='') as script_ivr_file:
                for line in script_ivr_file:
                    searchObj = re.search(r'.*URA_FASE=(.*?(\W))', line)
                    ura_fase = searchObj.group(1)[:-1] if searchObj else None
                    searchObj = re.search(r'.*RESP_INDEX=(.*?(\W))', line)
                    resp_index = searchObj.group(1)[:-1] if searchObj else None
                    if ura_fase and resp_index:
                        table.append([project_number, row['ScriptIVR'], ura_fase, resp_index])

    df = pd.DataFrame(table, columns=['campanha', 'script_ivr','ura_fase','resp_index'])
    df.to_csv(path_or_buf='resp.csv',sep='|',index=False,mode='w')
# df_server = df_agente_virtual.groupby(as_index=False, by=['IPExterno', 'PortaSSH']).count()
