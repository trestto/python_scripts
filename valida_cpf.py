import http.client
import json


from sqlalchemy import *
from sqlalchemy.ext.automap import automap_base
from sqlalchemy.sql import select

project_number = 503

engine = create_engine('mssql+pyodbc://ura:trt!@#H7v@200.201.128.218:1044/OPTIMUS?driver=SQL+Server+Native+Client+11.0')
conn = engine.connect()
trans = conn.begin()

conn = http.client.HTTPConnection("192.168.25.47:5006")

lista = [
['95050671353','950506713000053','85'],
['88393364191','883933641000091','85'],
['82648093320','826480933000020','85'],
['80996191372','809961913000072','85'],
['76663795287','766637952000087','85'],
['75731193304','757311933000004','85'],
['72283092272','722830922000072','85'],
['65621867220','656218672000020','85'],
['64202771249','642027712000049','85'],
['64086364387','640863643000087','85'],
['62121219315','621212193000015','85'],
['61651187398','616511873000098','85'],
['61231693355','612316933000055','85'],
['61228183309','612281833000009','85'],
['60849486343','608494863000043','85'],
['60724997326','607249973000026','85'],
['60423689312','604236893000012','85'],
['60166882313','601668823000013','85'],
['48733067368','487330673000068','85'],
['35501294320','355012943000020','85'],
['31426371845','314263718000045','85'],
['17882290378','178822903000078','85'],
['12640182153','126401821000053','85'],
['08704689798','087046897000098','85'],
['07166590424','071665904000024','85'],
['06773275369','067732753000069','85'],
['06653743489','066537434000089','85'],
['06404085321','064040853000021','85'],
['05572505951','055725059000051','85'],
['04860588320','048605883000020','85'],
['04848481350','048484813000050','85'],
['04351014393','043510143000093','85'],
['03426436302','034264363000002','85'],
['03229873343','032298733000043','85'],
['02611292361','026112923000061','85'],
['01042471347','010424713000047','85'],
['00081816324','000818163000024','85']
]

payload = """    {\n
                    \"usuario\": \"trestto\",\n
                    \"senha\": \"Parceir01\"\n        \n    } """

headers = {
    'content-type': "application/json",
    'cache-control': "no-cache",
    'postman-token': "5032ad93-bf19-9e87-5613-c553cef9e5f9"
    }

conn.request("POST", "/v1/autenticacao", payload, headers)

res = conn.getresponse()
data = json.loads(res.read().decode("utf-8"))
token = data['BearerToken']

for item in lista:
    payload = """{\"codCarteira\":\"%s\",
                    \"numeroDocumento\":\"%s\",
                    \"NegociacaoOpcaoContratos\":[{
                        \"CodContrato\":\"%s\"}],
                        \"codGrupoNeg\":\"PADRAO\",
                        \"tipoPessoa\":\"F\",
                        \"Plano\":\"1\",
                        \"DiaVenc\":13}""" %(item[2],item[0],item[1])

    headers = {
        'content-type': "application/json",
        'token': token,
        'cache-control': "no-cache",
        'postman-token': "2203aa51-4d87-cd92-5dfd-0cd0aae15587"
        }

    conn.request("POST", "/v1/negociacaoOpcao", payload, headers)

    res = conn.getresponse()
    data = json.loads(res.read().decode("utf-8"))

    if data['NegociacaoOpcoes']:
        sql = text("""  UPDATE [OPTIMUS].[dbo].[{project_number}_DadosCliente]
                        SET
                        cpf = '{cpf}'
                        ,CONTRATO = '{contrato}'
                        --NUMERO_CARTAO_CREDITO = '4487XXXXXXXX2119'
                        --DATA_VENCIMENTO_1 = '14/10/2017'
                        --Email = 'augusto.nascimento@trestto.com.br'
                        ,COD_CARTEIRA = {carteira}
                        WHERE IdCliente = 1""".format(project_number=project_number, cpf=item[0], contrato=item[1], carteira=item[2]))
        result = engine.execute(sql)
        print("""cpf = '{cpf}', CONTRATO = '{contrato}', COD_CARTEIRA = {carteira}""".format(cpf=item[0], contrato=item[1], carteira=item[2]))
        break
