import pyodbc
import pandas as pd
# from ftplib import FTP
import pysftp
from io import StringIO, BytesIO


def database_info ():
    return {
            '200.201.128.218:1044': {
                'NAME': 'OPTIMUS',
                'ENGINE': 'sql_server.pyodbc',
                'server': '200.201.128.218,1044',
                'uid': 'ivr_dev',
                'pwd': 'trestto@123',
                'OPTIONS':{'driver': 'SQL Server Native Client 11.0'}
            },
            '200.206.41.210:21744': {
                'NAME': 'OPTIMUS',
                'ENGINE': 'sql_server.pyodbc',
                'server': '200.206.41.210,21744',
                'uid': 'ivr_dev',
                'pwd': 'trestto@123',
                'OPTIONS':{'driver': 'SQL Server Native Client 11.0'}
            },
            '200.206.41.210:22744': {
                'NAME': 'OPTIMUS',
                'ENGINE': 'sql_server.pyodbc',
                'server': '200.206.41.210,22744',
                'uid': 'ivr_dev',
                'pwd': 'trestto@123',
                'OPTIONS':{'driver': 'SQL Server Native Client 11.0'}
            },

            '200.170.220.147:10130': {
                'NAME': 'OPTIMUS',
                'ENGINE': 'sql_server.pyodbc',
                'server': '200.170.220.147,10130',
                'uid': 'ivr_dev',
                'pwd': 'trestto@123',
                'OPTIONS':{'driver': 'SQL Server Native Client 11.0'}
            }
        }

def agente_virtual_info():

    database_list = database_info()

    frames = []
    for database in database_list.values():
        cn = pyodbc.connect(driver='{SQL Server Native Client 11.0}', server=database['server'], uid=database['uid'], pwd=database['pwd'])

        sql = """SELECT cs.Campanha,s.NomeMaquina,s.IP,s.IPExterno,s.PortaSSH, s.IDServidor ,c.ScriptIVR,c.Descricao
                FROM OPTIMUS.dbo.[ScriptServidor] cs
                INNER JOIN OPTIMUS.dbo.[Servidor] s ON cs.IdServidor = s.IDServidor
                INNER JOIN OPTIMUS.dbo.Campanha c ON c.campanha = cs.Campanha
                ORDER BY cs.Campanha
                """
        df = pd.read_sql(sql,cn)
        frames.append(df)

    return  pd.concat(frames)

cnopts = pysftp.CnOpts()
cnopts.hostkeys = None


df_agente_virtual = agente_virtual_info(client='intervalor')
df_server = df_agente_virtual.groupby(as_index=False, by=['IPExterno', 'PortaSSH']).count()

ftp_user_list = {
    '200.170.220.147': {
        2271:{'user':'root', 'pwd':'opt!@#H7v'},
        2272:{'user':'root', 'pwd':'opt!@#H7v'},
        2273:{'user':'root', 'pwd':'opt!@#H7v'},
        # 2274:{'user':'root', 'pwd':'opt!@#H7v'},
        2275:{'user':'root', 'pwd':'opt!@#H7v'},
        2276:{'user':'root', 'pwd':'opt!@#H7v'},
        2277:{'user':'root', 'pwd':'opt!@#H7v'},
        2278:{'user':'root', 'pwd':'opt!@#H7v'}
     },
    '200.201.128.218': {
        2261:{'user':'root', 'pwd':'opt!@#H7v'},
        2262:{'user':'root', 'pwd':'opt!@#H7v'},
        2264:{'user':'root', 'pwd':'opt!@#H7v'}
    }
}

for index, row in df_server.iterrows():
    print(row['IPExterno'], row['PortaSSH'])
    if row['IPExterno'] not in ftp_user_list.keys():
        print('ip nao econtrado')
        continue
    else:
        if int(row['PortaSSH']) not in ftp_user_list[row['IPExterno']].keys():
            print('porta nao encontrado')
            continue
    with pysftp.Connection(row['IPExterno'], username=ftp_user_list[row['IPExterno']][int(row['PortaSSH'])]['user'], password=ftp_user_list[row['IPExterno']][int(row['PortaSSH'])]['pwd'], port=int(row['PortaSSH']), cnopts=cnopts) as sftp:
        df_agente = df_agente_virtual.loc[(df_agente_virtual['IPExterno'] == row['IPExterno']) & (df_agente_virtual['PortaSSH'] == row['PortaSSH'])]
        for index2, row2 in df_agente.iterrows():
            path_source = '/home/optimus/ivr/ov/{campanha}/'.format(campanha=str(row2['Campanha'])[:3])
            file_name = "%s.conf" % (row2['ScriptIVR'])
            print(file_name)
            full_name = '{path_source}{file_name}'.format(path_source=path_source, file_name=file_name)
            if sftp.exists(full_name):
                path_destiny = 'C:/Users/augusto.nascimento/Documents/agente_virtual/ivr/ov/{campanha}/{file_name}'.format(campanha=str(row2['Campanha'])[:3], file_name=file_name)
                sftp.get(full_name, path_destiny)
            else:
                path_source = '/home/optimus/ivr/ov/{campanha}0/'.format(campanha=str(row2['Campanha'])[:3])
                full_name = '{path_source}{file_name}'.format(path_source=path_source, file_name=file_name)
                if sftp.exists(full_name):

                    path_destiny = 'C:/Users/augusto.nascimento/Documents/agente_virtual/ivr/ov/{campanha}0/{file_name}'.format(campanha=str(row2['Campanha'])[:3], file_name=file_name)
                    sftp.get(full_name, path_destiny)
            # ftp_download_file(ftp, row2['Campanha'], path, file_name)


# print(df_server)
