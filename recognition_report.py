import pandas as pd
import pyodbc
import xlwings as xw
import numpy as np
from settings import database_list, EmailUser
from scipy.stats import linregress

import smtplib
from os.path import basename
from email.mime.application import MIMEApplication
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.utils import COMMASPACE, formatdate


database = database_list['200.201.128.218:2020']



class ReportRecognition(object):
    """docstring for ."""
    def __init__(self):
        super(ReportRecognition, self).__init__()
        self.cn = pyodbc.connect(driver='{SQL Server Native Client 11.0}', server=database['server'], uid=database['uid'], pwd=database['pwd'])
        self.wb = xw.Book(r'C:/Users/augusto.nascimento/Documents/Recognition_Report.xlsm')
        self.wb.api.RefreshAll()

    def slope(self, row):
        b = row.dropna()
        a = range(1, b.count()+1)
        if a:
            return linregress(a, b).slope
        else:
            return np.NaN

    def sql_mensal(self, tipo):

        sql = """SELECT
                	r.{tipo},
                	r.mes,
                	r.reconhecimento,
                	r.qtd_reconhecimento,
                	r.qtd_total,
                	t.qtd qtd_tunning
                FROM (
                	SELECT
                			r.{tipo} {tipo},
                			cast(dbo.date_trunc(r.Data, 'month') as date) mes,
                		CASE
                					WHEN sum(r.qtd) = 0 THEN NULL
                					ELSE
                							CAST(SUM(CASE WHEN r.RecognitionStatus = '000' THEN r.qtd ELSE 0 END) AS numeric) / sum(r.qtd)
                			END reconhecimento,
                		SUM(CASE WHEN r.RecognitionStatus = '000' THEN r.qtd ELSE 0 END)  qtd_reconhecimento,
                		SUM(r.qtd) qtd_total
                	from tbl_recognition_report r
                	WHERE r.RecognitionStatus in ('000', '001') and r.campanha != '8888'
                	GROUP BY
                			r.{tipo},
                			dbo.date_trunc(r.Data, 'month')
                ) r
                LEFT JOIN (
                    SELECT
                        q1.mes,
                        q1.{tipo},
                        sum(q1.qtd) qtd
                    FROM (
                        SELECT
                                cast(dbo.date_trunc(u.DataAnalise, 'month') as date) mes,
                                u.Campanha campanha,
                                c.PontoReconhecimento context,
                                sum(cast(u.Analisado as int)) qtd
                        FROM [TELIUM_BD_REPORT].[TunningX].[dbo].[Utterance] u
                        INNER JOIN [TELIUM_BD_REPORT].[TunningX].[dbo].[PontoReconhecimento] c
                        on u.IdPontoReconhecimento = c.IdPontoReconhecimento
                        where
                                u.Analisado = 1
                                and u.DataAnalise >= '2018-01-01'
                        group by
                                cast(dbo.date_trunc(u.DataAnalise, 'month') as date),
                                u.Campanha,
                                c.PontoReconhecimento
                    ) q1
                    GROUP BY
                        q1.mes,
                        q1.{tipo}
                ) t
                ON r.mes = t.mes and r.{tipo} = t.{tipo}
                ORDER BY 1,2""".format(tipo=tipo)
        return sql

    def tabela_mensal(self, sql, tipo):

        df = pd.read_sql(sql,self.cn)
        t1 = pd.pivot_table(df, values='reconhecimento', index=tipo, columns='mes', margins=False)
        tunning = df.set_index([tipo,'mes'])['qtd_tunning']
        t2 = df.groupby(tipo)['qtd_reconhecimento', 'qtd_total'].sum()
        t2['consolidado'] = t2['qtd_reconhecimento'] / t2['qtd_total']
        t2['tendencia'] = t1[t1.columns[-3:]].apply(self.slope,axis=1)
        t2 = t2[['consolidado','tendencia']]
        # t2 = t2.to_frame()
        t = pd.concat([t1,t2], axis=1)

        return t, tunning

    def tabela_semanal(self, sql, tipo):

        df = pd.read_sql(sql,self.cn)
        t1 = pd.pivot_table(df, values='reconhecimento', index=tipo, columns='week', margins=False)
        tunning = df.set_index([tipo,'week'])['qtd_tunning']
        t2 = df.groupby(tipo)['qtd_reconhecimento', 'qtd_total'].sum()
        t2['consolidado'] = t2['qtd_reconhecimento'] / t2['qtd_total']
        t2['tendencia'] = t1[t1.columns[-4:]].apply(self.slope,axis=1)
        t2 = t2[['consolidado','tendencia']]
        # t2 = t2.to_frame()
        t = pd.concat([t1,t2], axis=1)

        return t, tunning

    def tabela_diario(self, sql, tipo):

        df = pd.read_sql(sql,self.cn)
        t1 = pd.pivot_table(df, values='reconhecimento', index=tipo, columns='data', margins=False)
        tunning = df.set_index([tipo,'data'])['qtd_tunning']
        t2 = df.groupby(tipo)['qtd_reconhecimento', 'qtd_total'].sum()
        t2['consolidado'] = t2['qtd_reconhecimento'] / t2['qtd_total']
        t2['tendencia'] = t1[t1.columns[-7:]].apply(self.slope,axis=1)
        t2 = t2[['consolidado','tendencia']]
        # t2 = t2.to_frame()
        t = pd.concat([t1,t2], axis=1)

        return t, tunning

    def tunning_mes(self, sht, tunning):
        left = 3
        top = 6
        right = sht.range((top,left+1)).end('right').column - 2
        down = sht.range((top,left)).end('down').row
        for row in range(top+1, down+1):
            indice_linha = sht.range(row, left).value
            if type(indice_linha) == type(float()):
                indice_linha = str(int(indice_linha))
            if indice_linha in tunning.keys():
                for column in range(left+1, right):
                    data = sht.range(top, column).value.strftime('%Y-%m-%d')
                    if data in tunning[indice_linha].keys():
                        if pd.notna(tunning[indice_linha][data]):
                            texto_tunning = "Tunning: \n quantidade de audios tratados: \n {tunning}".format(tunning=str(tunning[indice_linha][data]))
                            sht.range(row, column).api.AddComment(texto_tunning)
                            sht.range(row, column).color = (255,230,153)
                            # sht.range(row, column).api.Comment.Text(tunning[campanha][data])
            else:
                continue

    def tunning_semanal(self, sht, tunning):
        left = 3
        top = 6
        right = sht.range((top,left+1)).end('right').column - 2
        down = sht.range((top,left)).end('down').row
        for row in range(top+1, down+1):
            indice_linha = sht.range(row, left).value
            if type(indice_linha) == type(float()):
                indice_linha = str(int(indice_linha))
            if indice_linha in tunning.keys():
                for column in range(left+1, right):
                    data = sht.range(top, column).value
                    if data in tunning[indice_linha].keys():
                        if pd.notna(tunning[indice_linha][data]):
                            texto_tunning = "Tunning: \n quantidade de audios tratados: \n {tunning}".format(tunning=str(tunning[indice_linha][data]))
                            sht.range(row, column).api.AddComment(texto_tunning)
                            sht.range(row, column).color = (255,230,153)
                            # sht.range(row, column).api.Comment.Text(tunning[campanha][data])
            else:
                continue

    def tunning_diario(self, sht, tunning):
        left = 3
        top = 6
        right = sht.range((top,left+1)).end('right').column - 2
        down = sht.range((top,left)).end('down').row
        for row in range(top+1, down+1):
            indice_linha = sht.range(row, left).value
            if type(indice_linha) == type(float()):
                indice_linha = str(int(indice_linha))
            if indice_linha in tunning.keys():
                for column in range(left+1, right):
                    data = sht.range(top, column).value.strftime('%Y-%m-%d')
                    if data in tunning[indice_linha].keys():
                        if pd.notna(tunning[indice_linha][data]):
                            texto_tunning = "Tunning: \n quantidade de audios tratados: \n {tunning}".format(tunning=str(tunning[indice_linha][data]))
                            sht.range(row, column).api.AddComment(texto_tunning)
                            sht.range(row, column).color = (255,230,153)
                            # sht.range(row, column).api.Comment.Text(tunning[campanha][data])
            else:
                continue

    def contexto_mensal(self):
        print("contexto_mensal")
        sql = self.sql_mensal('context')
        t, tunning = self.tabela_mensal(sql, 'context')
        sht = self.wb.sheets['contexto_mensal']
        self.format_table(sht, t, 'mmm/aaaa', '0%')
        self.tunning_mes(sht, tunning)

    def campanha_mensal(self):
        print("campanha_mensal")
        sql = self.sql_mensal('campanha')
        t, tunning = self.tabela_mensal(sql, 'campanha')
        sht = self.wb.sheets['campanha_mensal']
        self.format_table(sht, t, 'mmm/aaaa', '0%')
        self.tunning_mes(sht, tunning)

    def contexto_semanal(self):
        print("contexto_semanal")
        sql = "SET DATEFIRST 1 SELECT * FROM fn_recognition_report_context_wekly() ORDER BY 1,2"
        t, tunning = self.tabela_semanal(sql, 'context')
        sht = self.wb.sheets['contexto_semanal']
        self.format_table(sht, t,'mmm/aaaa', '0%')
        self.tunning_semanal(sht, tunning)

    def campanha_semanal(self):
        print("campanha_semanal")
        sql = "SET DATEFIRST 1 SELECT * FROM fn_recognition_report_campaign_wekly() ORDER BY 1,2"
        t, tunning = self.tabela_semanal(sql, 'campanha')
        sht = self.wb.sheets['campanha_semanal']
        self.format_table(sht, t,'mmm/aaaa', '0%')
        self.tunning_semanal(sht, tunning)

    def contexto_diario(self):
        print("contexto_diario")
        sql = "SELECT * FROM fn_recognition_report_context_daily() ORDER BY 1,2"
        t, tunning = self.tabela_diario(sql, 'context')
        sht = self.wb.sheets['contexto_diario']
        self.format_table(sht, t,'dd/mmm', '0%')
        self.tunning_diario(sht, tunning)

    def campanha_diario(self):
        print("campanha_diario")
        sql = "SELECT * FROM fn_recognition_report_campaign_daily() ORDER BY 1,2"
        t, tunning = self.tabela_diario(sql, 'campanha')
        sht = self.wb.sheets['campanha_diario']
        self.format_table(sht, t,'dd/mmm', '0%')
        self.tunning_diario(sht, tunning)

    def format_table(self, sht, t, format_header, format_number):

        left = 3
        top = 6

        right = sht.range((top,left+1)).end('right').column
        down = sht.range((top,left)).end('down').row
        sht.range((top,left), (down, right)).clear()
        sht.range((top,left)).options(expand='table').value = t

        right = sht.range((top,left+1)).end('right').column
        down = sht.range((top,left)).end('down').row


        sht.range((top,left+1), (top,right)).number_format = format_header
        sht.range((top,left+1), (top,right)).api.HorizontalAlignment = -4108
        sht.range((top,left), (top,right)).api.VerticalAlignment = -4108
        sht.range((top,left), (top,right)).color = (180,198,231)
        sht.range((top,left), (top,right)).api.Font.Bold = True
        sht.range((top,left), (down,left)).api.Font.Bold = True

        sht.range((top+1,left+1), (down,right)).number_format = format_number

        sht.range((top,right-1), (down,right-1)).api.FormatConditions.Delete
        for border_id in range(7,13):
            sht.range((top,left), (down,right)).api.Borders(border_id).LineStyle=-4118
            sht.range((top,left), (down,right)).api.Borders(border_id).Weight=1
            sht.range((top,left), (down,right)).api.Borders(border_id).ThemeColor = 5
            sht.range((top,left), (down,right)).api.Borders(border_id).TintAndShade = 0.399945066682943

        values = []
        for l in range(top+1, down+1):
            if sht.range((l, right-2)).value:
                values.append(sht.range((l, right-2)).value)

        p_bottom_10 = np.percentile(values, 10)
        p_bottom_90 = np.percentile(values, 90)

        for l in range(top+1, down+1):
            if sht.range((l, right-2)).value:
                if sht.range((l, right-2)).value < p_bottom_10:
                    sht.range((l, right-2)).color = (255,199,206)
                    sht.range((l, right-2)).api.Font.Color = -16776961
                    sht.range((l, right-2)).api.Font.TintAndShade = 0
                elif sht.range((l, right-2)).value > p_bottom_90:
                    sht.range((l, right-2)).color = (198,239,206)
                    sht.range((l, right-2)).api.Font.Color = -16752384
                    sht.range((l, right-2)).api.Font.TintAndShade = 0

        values = []
        for l in range(top+1, down+1):
            if sht.range((l, right)).value:
                values.append(sht.range((l, right)).value)

        p_bottom_10 = np.percentile(values, 10)
        p_bottom_90 = np.percentile(values, 90)

        for l in range(top+1, down+1):
            if sht.range((l, right)).value:
                if sht.range((l, right)).value < p_bottom_10:
                    sht.range((l, right)).color = (255,199,206)
                    sht.range((l, right)).api.Font.Color = -16776961
                    sht.range((l, right)).api.Font.TintAndShade = 0
                elif sht.range((l, right)).value > p_bottom_90:
                    sht.range((l, right)).color = (198,239,206)
                    sht.range((l, right)).api.Font.Color = -16752384
                    sht.range((l, right)).api.Font.TintAndShade = 0

        sht.range((top,left), (down,right)).api.AutoFilter()
        sht.range((top,left), (down,right)).autofit()

    def save(self):
        self.wb.save()

    def send_email(self):


        recipients = ['augusto.nascimento@trestto.com.br', 'carlos@trestto.com.br', 'thomaz.sousa@trestto.com.br']
        msg = MIMEMultipart()
        msg['From'] = 'augusto.nascimento@trestto.com.br'
        msg['To'] = COMMASPACE.join(recipients)
        msg['Date'] = formatdate(localtime=True)
        msg['Subject'] = 'Relatorio de Reconhecimento'

        msg.attach(MIMEText('Segue em anexo Relatorio de Reconhecimento'))

        f = r'C:/Users/augusto.nascimento/Documents/Recognition_Report.xlsm'
        with open(f, "rb") as fil:
            part = MIMEApplication(
                fil.read(),
                Name=basename(f)
            )
        # After the file is closed
        part['Content-Disposition'] = 'attachment; filename="%s"' % basename(f)
        msg.attach(part)

        #put your host and port here
        s = smtplib.SMTP_SSL('email-ssl.com.br:465')
        s.login('augusto.nascimento@trestto.com.br','trestto@122')
        s.sendmail('augusto.nascimento@trestto.com.br', recipients, msg.as_string())
        s.quit()

if __name__ == '__main__':
    wb = xw.Book(r'C:/Users/augusto.nascimento/Documents/Recognition_Report.xlsm')
    RR = ReportRecognition()
    RR.campanha_mensal()
    RR.campanha_semanal()
    RR.campanha_diario()
    RR.contexto_mensal()
    RR.contexto_semanal()
    RR.contexto_diario()
    RR.save()
    RR.send_email()
