# -*- coding: iso-8859-1 -*
import pysftp
import os
import csv
import pyodbc
import pandas as pd
import paramiko
from settings import list_servers_config, database_list

cnopts = pysftp.CnOpts()
cnopts.hostkeys = None

directory_base_av = 'C:/Users/augusto.nascimento/Documents/agente_virtual'
directory_base_locutores = 'C:/Users/augusto.nascimento/Documents/locutores'
project_number = av_script.split('_')[1]
versao = av_script.split('_')[-1]

server_updated = []
server_not_updated = []

list_to_update = ['5240']

database = database_list['200.201.128.218:2020']

cn = pyodbc.connect(driver='{SQL Server Native Client 11.0}', server=database['server'], uid=database['uid'], pwd=database['pwd'])
sql = """EXEC optimus.dbo.sp_tools_campanha_servidor"""
df = pd.read_sql(sql,cn)
df = df.loc[df['Campanha'].isin(list_to_update)]

server_list = [x for x in list_servers_config if x['name'] in df.NomeMaquina.unique()]

for server in server_list:
    print(server)
    with pysftp.Connection(server['host'], username='root', password=server['pwd'], port=server['port_number'], cnopts=cnopts) as sftp:
        for index, project in df.loc[df['NomeMaquina']==server['name']].iterrows():
            #pasta do av
            src = '{directory_base_av}/ivr/ov/{project_number}/{av_script}.conf'.format(directory_base_av=directory_base_av,project_number=str(project['Campanha'])[:3],av_script=project['ScriptIVR'])
            dst = '{path_ov}/ov/{project_number}'.format(path_ov=server['path_ov'],project_number=str(project['Campanha'])[:3])
            print(src, dst)
            if sftp.isdir(dst):
                with sftp.cd(dst):
                    sftp.put(src, preserve_mtime=True)
                server_updated.append([server['name'],'av'])
            else:
                server_not_updated.append([server['name'],'av'])

            # #audios
            for locutor in ['gc','gs','jm','lb','mc','sf','ap','br','fa','ga']:
                src = '{directory_base_locutores}/pt_BR-{locutor}/{project_number}_prompts_{versao}'.format(directory_base_locutores=directory_base_locutores,locutor=locutor,project_number=str(project['Campanha'])[:3],versao=project['ScriptIVR'].split('_')[-1])
                dst = '{path_audio}/pt_BR-{locutor}/{project_number}_prompts_{versao}'.format(path_audio=server['path_audio'],locutor=locutor,project_number=str(project['Campanha'])[:3],versao=project['ScriptIVR'].split('_')[-1])
                if os.path.isdir(src):
                    print(src, dst)
                    if sftp.isdir(dst):
                        with sftp.cd(dst):
                            for f in os.listdir(src):
                                sftp.put(os.path.join(src,f), preserve_mtime=True)
                        server_updated.append([server['name'],'audios',locutor])
                    else:
                        server_not_updated.append([server['name'],'audios',locutor])

print('servidores atualizados')
print(server_updated)

print('servidores n�o atualizados')
print(server_not_updated)
