import xml.etree.ElementTree as ET

xml = """<?xml version="1.0" encoding="ISO8859-1"?>
<grammar xml:lang="pt-BR" version="1.0" xmlns="http://www.w3.org/2001/06/grammar" >
	<rule id="SUB_DIAS_ATRASO_V1" scope="public">
		<item repeat='0-1'>a</item>
		<one-of>
		    <item>quanto tempo</item>
			<item>quantos dias</item>
		</one-of>
		<one-of>
			<item>está</item>
			<item>consta</item>
			<item>consta de</item>
		</one-of>
		<one-of>
			<item>atraso</item>
			<item>atrasado</item>
			<item>vencido</item>
			<item>vencimento</item>
		</one-of>
	</rule>
</grammar>
"""

def parse(xml, ident):
    print('%s %s' %('   '*ident, xml.tag))
    for child in xml.getchildren():
        parse(child, ident+1)
xml = ET.fromstring(xml)
parse(xml,1)
