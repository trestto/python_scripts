HOST="192.168.25.58"
PORT=22

p = """Action: login
Events: off
Username: %(username)s
Secret: %(password)s

Action: originate
Channel: SIP/%(local_user)s
WaitTime: 60
CallerId: 600
Exten: %(phone_to_dial)s
Context: default
Priority: 1

Action: Logoff
"""
import socket
def click_to_call(phone_to_dial, username, password, local_user):
    pattern = p % {
            'phone_to_dial': phone_to_dial,
            'username': username,
            'password': password,
            'local_user': local_user}

    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect((HOST, PORT))

    data = s.recv(1024)
    for l in pattern.split('\n'):
        print ("Sending&gt;", l)
        s.send(l+'\r\n')
        if l == "":
            data = s.recv(1024)
            print (data)
    data = s.recv(1024)
    s.close()

if __name__ == '__main__':
    click_to_call(phone_to_dial='75001',
                  username='9006', password='tresttopbx!387',
                  local_user='9006')
