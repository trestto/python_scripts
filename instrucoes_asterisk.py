lista_instrucoes = {
'1':'       same => n,GoSub(TOCA_PROMPT_V2,s,1(fase,1))',
'2':'       same => n(retry),GoSub(TOCA_PROMPT_V2,s,1(fase,1))',
'3':'       same => n,GoSub(TOCA_PROMPT_V2,s,1(fase,1,,,1,4))',
'4':'       same => n(retry),GoSub(TOCA_PROMPT_V2,s,1(fase,1,,,1,4))',
'5':'       same => n,GoSub(TOCA_PROMPT_V2,s,1(fase,2))',
'6':'       same => n,GoSub(TOCA_PROMPT_V2,s,1(fase,3))',
'7':'       same => n,GoSub(TOCA_PROMPT_V2,s,1(fase,4))',
'8':'       same => n,GoSub(TOCA_PROMPT_V2,s,1(fase,5))',
'9':'       same => n,GoSub(TOCA_PROMPT_V2,s,1(fase,6))',
'10':'      same => n,GoSub(TOCA_PROMPT_V2,s,1(fase,7))',
'11':'      same => n,GoSub(EXECUTA_VOCALIZAR,s,1(nome,${HASH(DADOS_CLIENTE,NOME_VOCALIZAR)}))',
'12':'      same => n,GoSub(EXECUTA_VOCALIZAR,s,1(valor,${HASH(DADOS_CLIENTE,VALOR_DIVIDA)},,e))',
'13':'      same => n,GoSub(EXECUTA_VOCALIZAR,s,1(dia,${HASH(DADOS_CLIENTE,DIA_VENCIMENTO)},,2))',
'14':'      same => n,GoSub(EXECUTA_VOCALIZAR,s,1(mes,${HASH(DADOS_CLIENTE,MES_VENCIMENTO)},,de))',
'15':'      same => n,GoSub(EXECUTA_VOCALIZAR,s,1(semana,${HASH(DADOS_CLIENTE,DIA_SEMANA)},,para))'
}

lista_bd = {
'1':'200.201.128.218:2020',
'2':'200.201.128.218:1044',
'3':'200.206.41.210:21744',
'4':'200.206.41.210:22744',
'5':'200.170.220.147:10130',
'6':'10.100.31.70',
'7':'200.201.128.218:1818',
'8':'189.112.36.196:3510'
}


de_para_bd ={
'TELIUM_BD_10':'200.201.128.218:1044',
'TELIUM_BD_130':'200.170.220.147:10130',
'INTERVALOR':'10.100.31.70'
}


lista_locutor = ['gs','br','am','ap','em','fa','ga','gc','jm','lb','mc','sf']

lista_mes = {
'1':'Jan',
'2':'Fev',
'3':'Mar',
'4':'Abr',
'5':'Mai',
'6':'Jun',
'7':'Jul',
'8':'Ago',
'9':'Set',
'10':'Out',
'11':'Nov',
'12':'Dez',
'01':'Jan',
'02':'Fev',
'03':'Mar',
'04':'Abr',
'05':'Mai',
'06':'Jun',
'07':'Jul',
'08':'Ago',
'09':'Set',
}

list_servers_menu = [
#local_server,
    {'name':'HOMOLOGA','host':'192.168.25.93','port_number':22,'pwd':'opt!@#H7v','optimus':'opt', 'log_file':'full.OPTIMUS-CLOUD-01'},
    {'name':'OPTIMUS-INTERVALOR-01','host':'10.100.31.72','port_number':22,'pwd':'opt!@#H7v','optimus':'home', 'log_file':'full.SRVVAAPP02'},
    {'name':'OPTIMUS-INTERVALOR-02','host':'10.100.31.75','port_number':22,'pwd':'opt!@#H7v','optimus':'home', 'log_file':'full.SRVVAAPP04 '},
    {'name':'OPTIMUS-INTERVALOR-03','host':'10.100.31.77','port_number':22,'pwd':'opt!@#H7v','optimus':'opt', 'log_file':'full.SRVAAPP06'},
    {'name':'OPTIMUS-INTERVALOR-04','host':'10.100.31.78','port_number':22,'pwd':'opt!@#H7v','optimus':'opt', 'log_file':'full.SRVVAAPP07'},
    {'name':'OPTIMUS-INTERVALOR-05','host':'10.100.31.79','port_number':22,'pwd':'opt!@#H7v','optimus':'opt', 'log_file':'full.SRVVAAPP08'},
    {'name':'OPTIMUS-INTERVALOR-06','host':'10.100.31.80','port_number':22,'pwd':'opt!@#H7v','optimus':'opt', 'log_file':'full.SRVVAAPP09'},
    {'name':'OPTIMUS-INTERVALOR-07','host':'10.100.31.81','port_number':22,'pwd':'opt!@#H7v','optimus':'opt', 'log_file':'full.SRVVAAPP10'},
    {'name':'OPTIMUS-INTERVALOR-08','host':'10.100.31.82','port_number':22,'pwd':'opt!@#H7v','optimus':'opt', 'log_file':'full.SRVVAAPP11'},
    {'name':'OPTIMUS-CLOUD-01','host':'200.201.128.218','port_number':2261,'pwd':'opt!@#H7v','optimus':'home', 'log_file':'full.OPTIMUS-CLOUD-01'},
    #{'name':'OPTIMUS-CLOUD-02','host':'200.201.128.218','port_number':2262,'optimus':'home', 'log_file':'full'},
    {'name':'OPTIMUS-CLOUD-04','host':'200.201.128.218','port_number':2264,'pwd':'opt!@#H7v','optimus':'opt', 'log_file':'full.OPTIMUS-CLOUD-04'},
    {'name':'OPTIMUS-CLOUD-06','host':'200.201.128.218','port_number':2266,'pwd':'opt!@#H7v','optimus':'opt', 'log_file':'full'},
    {'name':'OPTIMUS-CLOUD-07','host':'200.201.128.218','port_number':2267,'pwd':'opt!@#H7v','optimus':'opt', 'log_file':'full.OPTIMUS-CLOUD-07'},
    {'name':'OPTIMUS-CLOUD-08','host':'200.201.128.218','port_number':2268,'pwd':'opt!@#H7v','optimus':'opt', 'log_file':'full.OPTIMUS-CLOUD-08'},
    ##{'name':'OPTIMUS-ULTRA-04','host':'192.168.4.44','port_number':22,'pwd':'Topt!@#H7v&','optimus':'home', 'log_file':'full.OPTIMUS-CLOUD-01'},
    ##{'name':'OPTIMUS-ULTRA-05','host':'192.168.4.45','port_number':22,'pwd':'Topt!@#H7v&','optimus':'home', 'log_file':'full.OPTIMUS-CLOUD-01'},
    ##{'name':'OPTIMUS-ULTRA-06','host':'192.168.4.46','port_number':22,'pwd':'Topt!@#H7v&','optimus':'opt', 'log_file':'full.OPTIMUS-CLOUD-01'},
    ##{'name':'OPTIMUS-ULTRA-07','host':'192.168.4.47','port_number':22,'pwd':'Topt!@#H7v&','optimus':'opt', 'log_file':'full.OPTIMUS-CLOUD-01'},
    ##{'name':'OPTIMUS-ULTRA-08','host':'192.168.4.48','port_number':22,'pwd':'Topt!@#H7v&','optimus':'home', 'log_file':'full.OPTIMUS-CLOUD-01'},
    ##{'name':'OPTIMUS-ULTRA-09','host':'192.168.4.49','port_number':22,'pwd':'Topt!@#H7v&','optimus':'opt', 'log_file':'full.OPTIMUS-CLOUD-01'},
    ##{'name':'OPTIMUS-ULTRA-10','host':'192.168.4.43','port_number':22,'pwd':'Topt!@#H7v&','optimus':'opt', 'log_file':'full.OPTIMUS-CLOUD-01'},
    ##{'name':'OPTIMUS-TELIUM-01','host':'200.170.220.147','port_number':2271,'pwd':'opt!@#H7v','optimus':'home', 'log_file':'full.OPTIMUS-TELIUM-01'},

    {'name':'OPTIMUS-TELIUM-02','host':'200.170.220.147','port_number':2272,'pwd':'opt!@#H7v','optimus':'home', 'log_file':'full.OPTIMUS-TELIUM-02'},
    {'name':'OPTIMUS-TELIUM-03','host':'200.170.220.147','port_number':2273,'pwd':'opt!@#H7v','optimus':'home', 'log_file':'full.OPTIMUS-TELIUM-03'},
    {'name':'OPTIMUS-TELIUM-06','host':'200.170.220.147','port_number':2276,'pwd':'opt!@#H7v','optimus':'home', 'log_file':'full.OPTIMUS-TELIUM-06'},
    {'name':'OPTIMUS-TELIUM-07','host':'200.170.220.147','port_number':2277,'pwd':'opt!@#H7v','optimus':'opt', 'log_file':'full.OPTIMUS-TELIUM-07'},
    {'name':'OPTIMUS-TELIUM-08','host':'200.170.220.147','port_number':2278,'pwd':'opt!@#H7v','optimus':'opt', 'log_file':'full.OPTIMUS-TELIUM-08'},
    {'name':'OPTIMUS-TELIUM-10','host':'200.170.220.147','port_number':2280,'pwd':'opt!@#H7v','optimus':'opt', 'log_file':'full.OPTIMUS-TELIUM-10'},
    ## {'name':'OPTIMUS-TRESTTO-03','host':'192.168.25.53','port_number':22150,'pwd':'opt!@#H7v','optimus':'home', 'log_file':'full'},
    ##{'name':'OPTIMUS-TRESTTO-04','host':'192.168.25.54','port_number':22,'pwd':'opt!@#H7v','optimus':'home', 'log_file':'full'},
    ##{'name':'OPTIMUS-SERVICES-01','host':'189.112.36.196','port_number':2251,'pwd':'opt!@#H7v','optimus':'opt', 'log_file':'full.OPTIMUS-SERVICES-01'}
]
