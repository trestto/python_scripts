# -*- coding: iso-8859-1 -*
import os, pyodbc, paramiko, xlwt, sys
from instrucoes_asterisk import *
from settings import database_list,list_servers_config
from datetime import date, datetime, time
import paramiko
from valida_dados import valida_projeto, valida_horario_log
import pysftp
#from fabric.api import *
#from fabric.contrib.files import exists,append,contains


def captura_log(clear,telefone,data,horario,num_projeto):
    separador = ('===' * 50)

    #if clear == 1:
    #    os.system ('cls')
    #    print ('')

    telefone = input ('Qual o telefone? \n')
    #telefone = '986555861'
    data = input ('Qual a data da ligacao? Exemplo: 14/07/2018 \n')
    #data = '13/08/2018'
    d = data.split("/")
    dia = d[0]
    mes = d[1]
    ano = d[2]

    horario_valido_i = '0'
    while horario_valido_i != '1':
        horario = input ("Qual o horario da ligacao? INSIRA APENAS A HORA - Exemplo: 10 \n")
        horario_valido_i = valida_horario_log(horario)
        if horario_valido_i == '1':
            print ('\n')
            break

    projeto_valido = '0'
    while projeto_valido != '1':
        num_projeto = input ("Qual o numero do projeto com 3 digitos? Exemplo: 581 \n")
        projeto_valido = valida_projeto(num_projeto)
        if projeto_valido == '1':
            print ('Numero do projeto correto \n')
            projeto = num_projeto+'0'

            break
    else:
        print ('Numero do projeto correto \n')

    if num_projeto == '306':
        projeto = num_projeto+'3'
    else:
        projeto = num_projeto+'0'


#=============== BUSCANDO STRING MES PARA PESQUISA DO LOG DA CHAMADA
    if str(mes) in lista_mes:
        mesnome = (lista_mes[''+str(mes)+''])

    else:
        print ('M�s invalido \n')
#====================== LOCALIZANDO SERVIDOR BANCO DE DADOS E ASTERISK =============================================================
#===================================================================================================================================

    print ('============= BUSCANDO DADOS DO PROJETO !! ================ \n')
    print ('===================== AGUARDE !!! ============================== \n')
    retorno = localiza_servidor(projeto)
    bd = retorno[0]
    asterisk = retorno[1]
    banco_report = retorno[2]
    print ('BANDO DE DADOS =',bd)
    print ('SERVIDOR ASTERISK = ',asterisk)
    print ('BANCO REPORT = ',banco_report)
    print ('\n')
#================= BUSCANDO REGISTRO NA TABELA HISTORICO CHAMADA ==================================================================================================================
#===================================================================================================================================
    print ('============== BUSCANDO OS REGISTROS NA TABELA HISTORICO CHAMADAS !! ================ \n')
    print ('===================== AGUARDE !!! ============================== \n')
    historico_chamada_painel,qtd_registros,tabela = historico_chamada(bd,telefone,projeto,dia,mes,ano,horario,data,banco_report)
    print ('A quantidade de registros do telefone '+telefone+' no dia '+dia+'/'+mes+'/'+ano+' eh:',qtd_registros)
    print ('\n')

    if qtd_registros == 0:
        print ('N�O FORAM LOCALIZADOS REGISTROS DE LIGA��O PARA ESSE N�MERO NA DATA INFORMADA !!')
        print ('RETORNANDO PARA O INICIO DO SCRIPT !! ')
        print ('\n \n')
        print (separador)
        print ('\n')

        return(0)
    else:
        if qtd_registros == 1:
            print (qtd_registros,' REGISTRO LOCALIZADO !! \n')
        else:
            print (qtd_registros,' REGISTROS LOCALIZADOS !! \n')

#===================================================================================================================================
#===================================================================================================================================
    print ('============== BUSCANDO LOG-ASTERISK DA CHAMADA  !! ================ \n')
    print ('===================== AGUARDE !!! ============================== \n')
    count = 0
    while count < int(qtd_registros):
        print ('============== BUSCANDO LOG-ASTERISK DA CHAMADA '+str(count)+'  !! ================ \n')
        print ('===================== AGUARDE !!! ============================== \n')
        print ('\n')
        print ('REGISTRO = ',count)

        if tabela == 'PAINEL':
            print ('A TABELA HISTORICO_CHAMADA_PAINEL N�O REGISTRA O UNIQUEID DA LIGA��O. \n')
        else:
            uniqueid = historico_chamada_painel[count][11]
            print ('UNIQUEID = ',uniqueid)

        idcliente = historico_chamada_painel[count][2]
        idligacaoura = historico_chamada_painel[count][-1]
        url_gravacao = historico_chamada_painel[count][12]
        print ('ID_CLIENTE = ',idcliente)
        print ('ID_LIGACAO_URA = ',idligacaoura)
        print ('URL_GRAVACAO = ',url_gravacao)
        print ('\n')


        print ('=============== SETANDO HORARIO DO LOG!! ================ \n')
        horario2 = str(historico_chamada_painel[count][4])
        print (horario2)
        hora_atual = datetime.now().strftime ("%H")

#==================== DEFININDO HORA LIMITE PARA LIGA��ES DO DIA ATUAL.

        if hora_atual < '10':
            hora_limite = '08'
        elif hora_atual < '12':
            hora_limite = '10'
        elif hora_atual < '14':
            hora_limite = '12'
        elif hora_atual < '16':
            hora_limite = '14'
        elif hora_atual < '18':
            hora_limite = '16'
        elif hora_atual < '20':
            hora_limite = '18'
        elif hora_atual < '22':
            hora_limite = '20'

        data_atual = date.today()
        dia_atual = data_atual.day
        mes_atual = data_atual.month
        print ('HORARIO ATUAL = ',hora_atual)
        print ('HORARIO DA CHAMADA = ',horario2[11:13])
        print ('MINUTO LIGACAO = ',horario2[14:16])
        print ('HORA LIMITE = ',hora_limite)
        print ('DIA ATUAL = ',dia_atual)
        print ('DIA LIGACAO = ',dia)


        if horario2[11:13] >= hora_limite and int(dia_atual) == int(dia):
            print ('SETANDO HORARIO ATUAL!')
            horario_log = hora_atual
            log_full_gz = 0
            print ('A HORA DO LOG SER� A HORA ATUAL = ', horario_log)
        else:
            if horario2[11:13] >= '08' and horario2[11:13] < '10':
                log_full_gz = 1
                horario_log = '10'
            elif horario2[11:13] >= '10' and horario2[11:13] < '12':
                log_full_gz = 1
                horario_log = '12'
            elif horario2[11:13] >= '12' and horario2[11:13] < '14':
                log_full_gz = 1
                horario_log = '14'
            elif horario2[11:13] >= '14' and horario2[11:13] < '16':
                log_full_gz = 1
                horario_log = '16'
            elif horario2[11:13] >= '16' and horario2[11:13] < '18':
                log_full_gz = 1
                horario_log = '18'
            elif horario2[11:13] >= '18' and horario2[11:13] < '20':
                log_full_gz = 1
                horario_log = '20'
            elif horario2[11:13] >= '20':
                log_full_gz = 1
                horario_log = '22'
            # LIGACOES AP�S AS 20 HRS S�O SALVAS NO LOG DAS 08 HRS DO DIA SEGUINTE

# ============= BUSCANDO LOG ASTERISK ====================
        print ('HORARIO LOG = ',horario_log)
        print ('\n')
        log_asterisk = registro_asterisk(dia,mesnome,asterisk,horario_log,telefone,ano,mes,horario2[11:13],log_full_gz,horario2[14:16],projeto)
        if log_asterisk == 0:
            print ('SERVIDOR ASTERISK N�O LOCALIZADO, O LOG DA LIGA��O N�O SER� GERADO! \nCONTINUANDO A EXECU��O DO SCRIPT!! \n')
            print (separador)

# ============= BUSCANDO LOG UTTERANCE
        log_utterance = utterance(projeto,idcliente,bd,idligacaoura,horario2[11:13],dia,mes,banco_report)
        #print ('retorno variavel log_utterance = ',log_utterance)
        if log_utterance == []:
            print ('============== N�O FORAM LOCALIZADOS REGISTROS UTTERANCE PARA ESSA LIGACAO ============== \n============== CONTINUANDO A EXECU��O DO SCRIPT !! ==============  \n')
            #captura_log(0)
        elif log_utterance == 0:
            print ('============== N�O FORAM LOCALIZADOS REGISTROS UTTERANCE PARA ESSA LIGACAO ============== \n============== CONTINUANDO A EXECU��O DO SCRIPT !! ==============  \n')
            #captura_log(0)
        else:
            print ('============== OS DADOS DA TABELA UTTERANCE EST�O SENDO SALVOS EM UMA PLANILHA!!! ============== \n')
            print ('===================== AGUARDE !!! ============================== \n')
            planilha_utterance = gera_planilha_utterance(log_utterance,url_gravacao,projeto)

        log_ws(bd,idcliente,idligacaoura,projeto)

        count = count + 1
    else:
        print (separador)
        print ('\n SCRIPT FINALIZADO, OS DADOS FORAM GERADOS E SALVOS NO DIRETORIO /TMP/ DO SERVIDOR E DIRETORIO DE EXECU��O DESSE SCRIPT \n ')
        print (separador)


    return (1)


def localiza_servidor(projeto):

    cn = pyodbc.connect(driver='{SQL Server Native Client 11.0}', server='200.201.128.218,2020', uid='rodolfo.dona', pwd='timeup4t2a3')
    sqlconsulta = """SELECT * from [optimus].[dbo].fn_tools_campanha_servidor() where campanha in ({projeto})""".format(projeto=projeto)

    result = cn.execute(sqlconsulta).fetchone()

    if result == None:
        print ('A consulta nao trouxe resultado, o projeto esta em outra base de dados. \n')
        print ('Consultando as informacoes no BD da Intervalor, Aguarde !! \n')
        cn = pyodbc.connect(driver='{SQL Server Native Client 11.0}', server='10.100.31.70', uid='rodolfo.dona', pwd='timeup4t2a3')
        sqlconsulta = """ SELECT * from [optimus].[dbo].[campanha] where campanha = {projeto}""".format(projeto=projeto)
        print (sqlconsulta)
        result2 = cn.execute(sqlconsulta).fetchone()
        if result2 != None:
            asterisk_projeto = result2[12]
            bd_projeto = 'INTERVALOR'
            banco_report = 'INTERVALOR'
            return(bd_projeto, asterisk_projeto,banco_report)
        else:
            print ('O projeto nao foi localizado em nenhuma base de dados. \n')

    else:
        dados = result
        bd_projeto = dados[0]
        asterisk_projeto = dados[1]
        banco_report = dados[10]
        return (bd_projeto,asterisk_projeto,banco_report)

def historico_chamada(bd,telefone,projeto,dia,mes,ano,horario,data_chamada,banco_report):

    data_atual = date.today()
    dia_atual = data_atual.day
    mes_atual = data_atual.month
    ano_atual = data_atual.year
    hora_atual = datetime.now().strftime ("%H-%M")

    if bd in de_para_bd:
        ip_bd = (de_para_bd[''+bd+''])
        print ('IP BANCO DE DADOS: ',ip_bd)
        print ('\n')
    else:
        ip_bd = '10.100.31.70'
        print ('O ip do banco de dados (INTERVALOR) eh: ',ip_bd)
        print ('\n')



    if int(mes) < int(mes_atual):
        tabela_historico = 'HST'
    elif int(dia) < int(dia_atual):
        tabela_historico = 'HST'
    else:
        tabela_historico = 'PAINEL'


    if tabela_historico == 'HST':

        if ip_bd == '10.100.31.70':
            database = '10.100.31.70'
        else:
            database = '200.201.128.218,1818'
        # setando acesso ao banco de dados de report.

        cn = pyodbc.connect(driver='{SQL Server Native Client 11.0}', server=database, uid='rodolfo.dona', pwd='timeup4t2a3')

        print ('PESQUISANDO REGISTROS NA TABELA HISTORICO_CHAMADA_HST \n')

        if database == '10.100.31.70':
            print ("SELECT count(*) from [optimus_hst].[dbo].[historicochamada_hst] with (nolock) where calldate > '{ano}-{mes}-{dia} {horario}:00:00:000'  and telefone = '{telefone}' and Campanha = {campanha} and statusligacao = 'ANSWERED'""".format(telefone=telefone,campanha=projeto,dia=dia,mes=mes,ano=ano,horario=horario))
            sql_qtd_registros = """SELECT count(*) from [optimus_hst].[dbo].[historicochamada_hst] with (nolock) where calldate > '{ano}-{mes}-{dia} {horario}:00:00:000' and  calldate < '{ano}-{mes}-{dia} 23:59:59:000'  and telefone = '{telefone}' and Campanha = {campanha} and statusligacao = 'ANSWERED'""".format(telefone=telefone,campanha=projeto,dia=dia,mes=mes,ano=ano,horario=horario)

            result_qtd = cn.execute(sql_qtd_registros).fetchone()
            retorno = result_qtd

            parametros = 'idhistoricochamadahst,idhistoricochamada,idcliente,idlista,calldate,ddd,telefone,link,datahoraatendeu,datahorafim,duracao,uniqueid,urlgravacao,statusura,faseura,substatusura,statusligacao,resp_1,resp_2,resp_3,resp_4,resp_5,resp_6,resp_7,resp_8,resp_9,resp_10,resp_11,resp_12,resp_13,resp_14,resp_15,codfinalizacao,statusintegracao,nome,cpf,codigocliente,idligacaoura'

            print ("""SELECT {parametros} from [optimus_hst].[dbo].[historicochamada_hst] with (nolock) where calldate > '{ano}-{mes}-{dia} {horario}:00:00:000' and calldate < '{ano}-{mes}-{dia} 23:59:59:000' and telefone = '{telefone}' and Campanha = {campanha} and statusligacao = 'ANSWERED'""".format(telefone=telefone,campanha=projeto,parametros=parametros,dia=dia,mes=mes,ano=ano,horario=horario))
            sql_consulta = """SELECT {parametros} from [optimus_hst].[dbo].[historicochamada_hst] with (nolock) where calldate > '{ano}-{mes}-{dia} {horario}:00:00:000' and calldate < '{ano}-{mes}-{dia} 23:59:59:000' and telefone = '{telefone}' and Campanha = {campanha} and statusligacao = 'ANSWERED'""".format(telefone=telefone,campanha=projeto,parametros=parametros,dia=dia,mes=mes,ano=ano,horario=horario)
            result = cn.execute(sql_consulta).fetchall()
            return result,retorno[0],tabela_historico
        else:
            print ("SELECT count(*) from [{banco_report}].[dbo].[historicochamada_hst] with (nolock) where calldate > '{ano}-{mes}-{dia} {horario}:00:00:000'  and telefone = '{telefone}' and Campanha = {campanha} and statusligacao = 'ANSWERED'""".format(telefone=telefone,campanha=projeto,dia=dia,mes=mes,ano=ano,horario=horario,banco_report=banco_report))
            sql_qtd_registros = """SELECT count(*) from [{banco_report}].[dbo].[historicochamada_hst] with (nolock) where calldate > '{ano}-{mes}-{dia} {horario}:00:00:000' and  calldate < '{ano}-{mes}-{dia} 23:59:59:000'  and telefone = '{telefone}' and Campanha = {campanha} and statusligacao = 'ANSWERED'""".format(telefone=telefone,campanha=projeto,dia=dia,mes=mes,ano=ano,horario=horario,banco_report=banco_report)

            result_qtd = cn.execute(sql_qtd_registros).fetchone()
            retorno = result_qtd

            parametros = 'idhistoricochamadahst,idhistoricochamada,idcliente,idlista,calldate,ddd,telefone,link,datahoraatendeu,datahorafim,duracao,uniqueid,urlgravacao,statusura,faseura,substatusura,statusligacao,resp_1,resp_2,resp_3,resp_4,resp_5,resp_6,resp_7,resp_8,resp_9,resp_10,resp_11,resp_12,resp_13,resp_14,resp_15,codfinalizacao,statusintegracao,nome,cpf,codigocliente,idligacaoura'

            print ("""SELECT {parametros} from [{banco_report}].[dbo].[historicochamada_hst] with (nolock) where calldate > '{ano}-{mes}-{dia} {horario}:00:00:000' and calldate < '{ano}-{mes}-{dia} 23:59:59:000' and telefone = '{telefone}' and Campanha = {campanha} and statusligacao = 'ANSWERED'""".format(telefone=telefone,campanha=projeto,parametros=parametros,dia=dia,mes=mes,ano=ano,horario=horario,banco_report=banco_report))
            sql_consulta = """SELECT {parametros} from [{banco_report}].[dbo].[historicochamada_hst] with (nolock) where calldate > '{ano}-{mes}-{dia} {horario}:00:00:000' and calldate < '{ano}-{mes}-{dia} 23:59:59:000' and telefone = '{telefone}' and Campanha = {campanha} and statusligacao = 'ANSWERED'""".format(telefone=telefone,campanha=projeto,parametros=parametros,dia=dia,mes=mes,ano=ano,horario=horario,banco_report=banco_report)
            result = cn.execute(sql_consulta).fetchall()
            return result,retorno[0],tabela_historico
    else:
        print ('PESQUISANDO REGISTROS NA TABELA HISTORICO CHAMADA PAINEL \n')

        database = database_list[ip_bd]
        #print ('Variavel database = ',database)
        cn = pyodbc.connect(driver='{SQL Server Native Client 11.0}', server=database['server'], uid='ura', pwd='trt!@#H7v')

        print ("""SELECT count(*) from [optimus].[dbo].[historicochamadapainel] with (nolock) where calldate > '{ano}-{mes}-{dia} {horario}:00:00:000' and calldate < '{ano}-{mes}-{dia} 23:59:59:000' and telefone = '{telefone}' and Campanha = {campanha} and statusligacao = 'ANSWERED'""".format(telefone=telefone,campanha=projeto,dia=dia,mes=mes,ano=ano,horario=horario))
        sql_qtd_registros = """SELECT count(*) from [optimus].[dbo].[historicochamadapainel] with (nolock) where calldate > '{ano}-{mes}-{dia} {horario}:00:00:000' and calldate < '{ano}-{mes}-{dia} 23:59:59:000' and telefone = '{telefone}' and Campanha = {campanha} and statusligacao = 'ANSWERED'""".format(telefone=telefone,campanha=projeto,dia=dia,mes=mes,ano=ano,horario=horario)
        result_qtd = cn.execute(sql_qtd_registros).fetchone()
        retorno = result_qtd
        parametros = 'idhistoricochamadapainel,idhistoricochamada,idcliente,idlista,calldate,ddd,telefone,link,datahoraatendeu,datahorafim,duracao,codagente,urlgravacao,statusura,faseura,substatusura,statusligacao,resp_1,resp_2,resp_3,resp_4,resp_5,resp_6,resp_7,resp_8,resp_9,resp_10,resp_11,resp_12,resp_13,resp_14,resp_15,codfinalizacao,statusintegracao,nome,cpf,codigocliente,idligacaoura'
        print ("""SELECT {parametros} from [optimus].[dbo].[historicochamadapainel] with (nolock) where calldate > '{ano}-{mes}-{dia} {horario}:00:00:000' and calldate < '{ano}-{mes}-{dia} 23:59:59:000' and telefone = '{telefone}' and Campanha = {campanha} and statusligacao = 'ANSWERED'""".format(telefone=telefone,campanha=projeto,parametros=parametros,dia=dia,mes=mes,ano=ano,horario=horario))
        sql_consulta = """SELECT {parametros} from [optimus].[dbo].[historicochamadapainel] with (nolock) where calldate > '{ano}-{mes}-{dia} {horario}:00:00:000' and calldate < '{ano}-{mes}-{dia} 23:59:59:000' and telefone = '{telefone}' and Campanha = {campanha} and statusligacao = 'ANSWERED'""".format(telefone=telefone,campanha=projeto,parametros=parametros,dia=dia,mes=mes,ano=ano,horario=horario)
        result = cn.execute(sql_consulta).fetchall()

        return result,retorno[0],tabela_historico

def registro_asterisk(dia,mes_str,asterisk,horario_log,telefone,ano,mes,horario,log_full_gz,minuto,projeto):


    for server in list_servers_menu:
        if asterisk == server['name']:
            host = server['host']
            port = server['port_number']
        elif asterisk == server['host']:
            host = server['host']
            port = server['port_number']

    if host == None:
        print ('O SERVIDOR ASTERISK N�O FOI LOCALIZADO NA LISTA DE SERVIDORES, FAVOR VERIFICAR A LISTA DE SERVERS !! \n')
        return 0

    if host == '':
        print ('O SERVIDOR ASTERISK N�O FOI LOCALIZADO NA LISTA DE SERVIDORES, FAVOR VERIFICAR A LISTA DE SERVERS !! \n')
        return 0

    print ('IP ASTERISK = ',host)
    print ('PORTA SSH = ',port)


    print ('============== CONECTANDO NO SERVIDOR ASTERISK PARA CAPTURAR O LOG DA CHAMADA ============== \n')
    print ('============================ AGUARDE !! ============================ \n')




    # id_rsa, necessario exportar chave ssh no puttygen utilizando a chave de acesso padr�o.
    #Just a note, if you have generated your keys via puttygen and they are in ppk format, it's not going to work, go back to puttygen, load you ppk key and export it as OpenSSH (Conversion -> Export OpenSSH Key)
    chave_ssh = os.path.expanduser('C:/Users/rodolfo.dona/Desktop/chaves/id_rsa.ppk')
    paramiko.util.log_to_file('C:/Users/rodolfo.dona/Desktop/chaves/ssh.log')  # sets up logging
    paramiko.SSHException('not a valid RSA private key file',)
    client = paramiko.SSHClient()
    client.set_missing_host_key_policy(paramiko.AutoAddPolicy())

    client.connect(
        host,
        port=port,
        username='root',
        password='opt!@#H7v',
        key_filename = chave_ssh)

    #ajustando o horario do log de ligacoes ap�s as 20 hrs.
    if horario_log == '22':
        dia_ls = int(dia)+1
        horario_log = '08'
    else:
        dia_ls = dia

    ## alterando o primeiro digito da data para consulta no LS -LH, pois o linux salva o arquivo sem o 0 para datas entre 01 e 09.
    if dia_ls[0] == '0':
        print ('Alterando data do arquivo !!!!')
        dia_ls = dia_ls.replace('0',' ')

    if log_full_gz == 1:
        print ("ls -lh /var/log/asterisk/full.*.gz | grep '{mes} {dia}' | grep {hora}:".format(dia=dia_ls, hora=horario_log,mes=mes_str))
        stdin, stdout, stderr = client.exec_command("ls -lh /var/log/asterisk/full.*.gz | grep '{mes} {dia}' | grep {hora}:".format(dia=dia_ls, hora=horario_log,mes=mes_str))
        arq = stdout.readlines(0)
        arq2 = str(arq)
    else:
        print ("ls -lh /var/log/asterisk/full.* | grep '{mes} {dia}' | grep {hora}:".format(dia=dia_ls, hora=horario_log,mes=mes_str))
        stdin, stdout, stderr = client.exec_command("ls -lh /var/log/asterisk/full.* | grep '{mes} {dia}' | grep {hora}:".format(dia=dia_ls, hora=horario_log,mes=mes_str))
        arq = stdout.readlines(0)
        arq2 = str(arq)



    if arq2 == '[]':
        print ('O ARQUIVO FULL N�O FOI LOCALIZADO!! \nCONTINUANDO A EXECU��O DO SCRIPT !! \n')
        return 0

    if log_full_gz == 1:
        full = arq2.split("/")
        full2 = full[4].split(".gz")

        print ('ARQUIVO FULL = ',full2[0])
        print ('\n')
        print ('COMANDO EXECUTADO PARA LEITURA DO ARQUIVO FULL \n')
        print ("zcat /var/log/asterisk/{full}.gz | grep {telefone} | grep pbx.c | grep '{ano}-{mes}-{dia} {hora}:{minuto}'".format(telefone=telefone,full=full2[0],ano=ano,mes=mes,dia=dia,hora=horario,minuto=minuto))
        stdin, stdout, stderr = client.exec_command("zcat /var/log/asterisk/{full}.gz | grep {telefone} | grep pbx.c | grep '{ano}-{mes}-{dia} {hora}:{minuto}'".format(telefone=telefone,full=full2[0],ano=ano,mes=mes,dia=dia,hora=horario,minuto=minuto))
        retorno = stdout.readlines(0)
    else:
        full = arq2.split("/")
        #print (full)
        full2 = full[4].split(".")[1]
        #print (full2)
        full3 = full2.split("n']")[0]
        #print (full3)
        full4 = full3[:-1]
        #full2 = full[4].split(".gz")

        print ('ARQUIVO FULL = ',full4)
        print ('\n')
        print ('COMANDO EXECUTADO PARA LEITURA DO ARQUIVO FULL \n')
        print ("cat /var/log/asterisk/full.{full} | grep {telefone} | grep pbx.c | grep '{ano}-{mes}-{dia} {hora}:{minuto}'".format(telefone=telefone,full=full4,ano=ano,mes=mes,dia=dia,hora=horario,minuto=minuto))
        stdin, stdout, stderr = client.exec_command("cat /var/log/asterisk/full.{full} | grep {telefone} | grep pbx.c | grep '{ano}-{mes}-{dia} {hora}:{minuto}'".format(telefone=telefone,full=full4,ano=ano,mes=mes,dia=dia,hora=horario,minuto=minuto))
        retorno = stdout.readlines(0)
        print ('O RETORNO DO COMANDO EH = ',retorno)


    l = str(retorno)
    s_horario = l.split("] VERBOSE")
    s_horario2 = s_horario[0].split("'[")
    #print ('\n')
    #print ('O HORARIO DA LIGA��O NO LOG � = ',s_horario2[1])
    separa = l.split("][")
    separa2 = separa[1].split("] pbx.c")
    codigo = separa2[0]
    print ('\n')
    print ('O CODIGO DA LIGA��O NO ARQUIVO FULL �: ',codigo)
    print ('\n')
    print ('============== SALVANDO LOG DA CHAMADA NO DIRETORIO /TMP/  !! ================ \n')
    print ('===================== AGUARDE !!! ============================== \n')

    if log_full_gz == 0:
        stdin, stdout, stderr = client.exec_command('cat /var/log/asterisk/full.{full} | grep {codigo} > /tmp/log_{telefone}_{horario}-{minuto}hrs_{codigo}.txt'.format(codigo=codigo,full=full4,telefone=telefone,horario=horario,minuto=minuto))
        log_codigo = stdout.readlines(0)

    else:
        stdin, stdout, stderr = client.exec_command('zcat /var/log/asterisk/{full}.gz | grep {codigo} > /tmp/log_{telefone}_{horario}-{minuto}hrs_{codigo}.txt'.format(codigo=codigo,full=full2[0],telefone=telefone,horario=horario,minuto=minuto))
        log_codigo = stdout.readlines(0)


    if os.path.isdir("C:/Users/rodolfo.dona/Documents/Projetos/LOGS/"+projeto+""):
        print ('Pasta do projeto ja existe!! \n')
    else:
        print ('Criando pasta do projeto!! \n')
        os.mkdir("C:/Users/rodolfo.dona/Documents/Projetos/LOGS/"+projeto+"")
        print ('Pasta criada! \n')

    remotepath = '/tmp/log_{telefone}_{horario}-{minuto}hrs_{codigo}.txt'.format(codigo=codigo,full=full2[0],telefone=telefone,horario=horario,minuto=minuto)
    localpath = 'C:/Users/rodolfo.dona/Documents/Projetos/LOGS/'+projeto+'/log_{telefone}_{horario}-{minuto}hrs_{codigo}.txt'.format(codigo=codigo,full=full2[0],telefone=telefone,horario=horario,minuto=minuto)
    #localpath = 'C:/Users/rodolfo.dona/Documents/Projetos/'
    print ('================ TRANSFERINDO LOG =================  \n')
    sftp = client.open_sftp()
    sftp.get(remotepath,localpath)
    print ('================ LOG TRANSFERIDO PARA O COMPUTADOR LOCAL =================  \n')
    sftp.close()
    print ('\n')
    print ("LOG DA LIGA��O TAMB�M EST� SALVO NO DIRETORIO /tmp/log_{telefone}_{horario}:{minuto}hrs_{codigo}.txt".format(codigo=codigo,full=full2[0],telefone=telefone,horario=horario,minuto=minuto))


def utterance(projeto,idcliente,bd,idligacaoura,horario2,dia,mes,banco_report):
    data_atual = date.today()
    dia_atual = data_atual.day
    mes_atual = data_atual.month
    ano_atual = data_atual.year

    print ('============== BUSCANDO REGISTRO UTTERANCE  !! ================ \n')
    print ('===================== AGUARDE !!! ============================== \n')

    if bd in de_para_bd:
        ip_bd = (de_para_bd[''+bd+''])
        print ('IP BANCO DE DADOS: ',ip_bd)
        print ('\n')
    else:
        ip_bd = '10.100.31.70'
        print ('O ip do banco de dados (INTERVALOR) eh: ',ip_bd)
        print ('\n')

    if int(mes) < int(mes_atual):
        tabela_historico = 'HST'
    elif int(dia) < int(dia_atual):
        tabela_historico = 'HST'
    else:
        tabela_historico = 'UTTERANCE'

    if tabela_historico == 'HST':
        print ('PESQUISANDO REGISTROS NA TABELA UTTERANCE_HST \n')

        if ip_bd == '10.100.31.70':
            database = '10.100.31.70'
        else:
            database = '200.201.128.218,1818'

        cn = pyodbc.connect(driver='{SQL Server Native Client 11.0}', server=database, uid='rodolfo.dona', pwd='timeup4t2a3')

        if ip_bd == '10.100.31.70':
            parametros = '[IdCliente],[idLigacao],convert (date, [Data]) Dia,[Hora],[Campanha],[FaseUra],[Context],[Confidence],[Score],[RecognitionStatus],[Return],[Userinput],[NomeURA],[NomeArquivo]'
            sql_consulta = """SELECT {parametros} from [optimus_hst].[dbo].[utterance_hst] with (nolock) where campanha = {campanha} and idcliente = {idcliente} and idligacao = {idligacao} order by hora""".format(idcliente=idcliente,campanha=projeto,parametros=parametros,idligacao=idligacaoura)
            result = cn.execute(sql_consulta).fetchall()
            #print ('A quantidade de registros eh = ',qtd_reg)
            return result
        else:
            parametros = '[IdCliente],[idLigacao],convert (date, [Data]) Dia,[Hora],[Campanha],[FaseUra],[Context],[Confidence],[Score],[RecognitionStatus],[Return],[Userinput],[NomeURA],[NomeArquivo]'
            sql_consulta = """SELECT {parametros} from [{banco_report}].[dbo].[utterance_hst] with (nolock) where campanha = {campanha} and idcliente = {idcliente} and idligacao = {idligacao} order by hora""".format(idcliente=idcliente,campanha=projeto,parametros=parametros,idligacao=idligacaoura,banco_report=banco_report)
            result = cn.execute(sql_consulta).fetchall()
            #print ('A quantidade de registros eh = ',qtd_reg)
            return result
    else:
        print ('PESQUISANDO REGISTROS NA TABELA UTTERANCE \n')
        database = database_list[ip_bd]
        print ('ip bd = ',database['server'])

        cn = pyodbc.connect(driver='{SQL Server Native Client 11.0}', server=database['server'], uid='rodolfo.dona', pwd='timeup4t2a3')

        parametros = '[IdCliente],[idLigacao],convert (date, [Data]) Dia,[Hora],[Campanha],[FaseUra],[Context],[Confidence],[Score],[RecognitionStatus],[Return],[Userinput],[NomeURA],[NomeArquivo]'
        sql_consulta = """SELECT {parametros} from [optimus].[dbo].[utterance] with (nolock) where campanha = {campanha} and idcliente = {idcliente} and idligacao = {idligacao} order by hora""".format(idcliente=idcliente,campanha=projeto,parametros=parametros,idligacao=idligacaoura)
        result = cn.execute(sql_consulta).fetchall()
        return result

def gera_planilha_utterance(log_utterance,url_gravacao,projeto):

    separador = ('===' * 50)
    conta_linhas = len(log_utterance)
    reg = log_utterance
    print ('QUANTIDADE DE REGISTROS UTTERANCE = ',conta_linhas)
    print ('ID_CLIENTE = ',reg[0][0])
    print ('ID_LIGACAO = ',reg[0][1])
    print ('\n \n')

    if os.path.isdir("C:/Users/rodolfo.dona/Documents/Projetos/LOGS/"+projeto+""):
        print ('Pasta do projeto ja existe!! \n')
    else:
        print ('Criando pasta do projeto!! \n')
        os.mkdir("C:/Users/rodolfo.dona/Documents/Projetos/LOGS/"+projeto+"")
        print ('Pasta criada! \n')

    dest_arq = 'C:/Users/rodolfo.dona/Documents/Projetos/LOGS/'+projeto+'/'

    # DEFININDO PLANILHA
    wb = xlwt.Workbook()
    ws = wb.add_sheet('REGISTRO_UTTERANCE')

    # TITULO DAS COLUNAS
    titles = ['ID_CLIENTE','ID_LIGACAO','DIA','HORA','CAMPANHA','FASEURA','PONTO_DE_RECONHECIMENTO','CONFIDENCE','SCORE','RECOGNITION_STATUS','SAIDA','RESPOSTA_DO_USUARIO','NOME_URA','GRAVACAO_RESPOSTA','GRAVACAO_COMPLETA']

    # DEFINDO A FONTE UTILIZADA NA TABELA
    style0 =xlwt.easyxf('font: name Times New Roman, color-index red, bold on')
    # DEFININDO O FORMATO DA DATA
    style1 = xlwt.XFStyle()
    style1.num_format_str = 'dd/mm/yyyy'
    # DEFININDO O FOMRATO DO HORARIO
    style2 = xlwt.XFStyle()
    style2.num_format_str = 'hh:mm:ss'


    # ESCREVENDO O TITULO DAS COLUNAS NO ARQUIVO
    for i in range(len(titles)):
        #       lin,col,conteudo, formatacao
        ws.write(0, i, titles[i], style0)

    # Definindo largura das c�lulas das sequ�ncia
    for i in range(1,50):
        ws.col(i).width = 512
        i = i+ 1

    # INSERINDO RETORNOS DO UTTERANCE NA PLANILHA
    r = 0
    l = 1
    c = 0
    for a in range(len(reg)):
        ws.write(l,c,reg[r][0])
        c = c+1
        ws.write(l,c,reg[r][1])
        c = c+1
        ws.write(l,c,reg[r][2], style1)
        c = c+1
        ws.write(l,c,reg[r][3], style2)
        c = c+1
        ws.write(l,c,reg[r][4])
        c = c+1
        ws.write(l,c,reg[r][5])
        c = c+1
        ws.write(l,c,reg[r][6])
        c = c+1
        ws.write(l,c,reg[r][7])
        c = c+1
        ws.write(l,c,reg[r][8])
        c = c+1
        ws.write(l,c,reg[r][9])
        c = c+1
        ws.write(l,c,reg[r][10])
        c = c+1
        ws.write(l,c,reg[r][11])
        c = c+1
        ws.write(l,c,reg[r][12])
        c = c+1
        ws.write(l,c,reg[r][13])

        l = l+1
        c = 0
        r = r+1
    #ADICIONANDO A URL DA GRAVA��O DA CHAMADA NA PRIMEIRA LINHA E ULTIMA COLUNA DO ARQUIVO.
    ws.write(1,14,url_gravacao)

    wb.save(''+dest_arq+'UTTERANCE_ID_CLIENTE_'+str(reg[0][1])+'.xls')

    print ('PLANILHA GERADA COM SUCESSO!! \n\nVERIFIQUE NO DIRETORIO C:/Users/rodolfo.dona/Documents/Projetos/LOGS/'+projeto+'/UTTERANCE_ID_CLIENTE_'+str(reg[0][1])+'.xls \n')

    return

def log_ws(bd,idcliente,idligacaoura,projeto):

    print ('\n')
    print ('============== BUSCANDO REGISTRO WEB SERVICE !! ================ \n')
    print ('===================== AGUARDE !!! ============================== \n')


    if bd in de_para_bd:
        ip_bd = (de_para_bd[''+bd+''])
    else:
        ip_bd = '10.100.31.70'

    database = database_list[ip_bd]

    cn = pyodbc.connect(driver='{SQL Server Native Client 11.0}', server=database['server'], uid='rodolfo.dona', pwd='timeup4t2a3')

    parametro1 = 'rec.datahora_request, rec.path,rec.dados_enviados,resp.dados_enviados,'
    parametro2 = 'rec.id_log_web_request = resp.id_log_web_request'
    sql_consulta = """SELECT {parametro1}* FROM optimus.dbo.log_web_request rec INNER JOIN optimus.dbo.log_web_response resp ON {parametro2} WHERE id_cliente = {idcliente} AND campanha = {campanha} AND id_ligacao = {idligacaoura} ORDER BY 1""".format(idcliente=idcliente,idligacaoura=idligacaoura,campanha=projeto,parametro1=parametro1,parametro2=parametro2)
    result = cn.execute(sql_consulta).fetchall()

    if result == []:
        print ('ESSA LIGA��O N�O POSSUI REGISTROS DE INTERA��O COM WEBSERVICE !!')
        return
    else:
        gera_planilha_log_ws(result,idcliente,projeto)
        return


def gera_planilha_log_ws(result,idcliente,projeto):

    print ('\n')
    print ('============== GERANDO PLANILHA COM OS REGISTROS DO WEB SERVICE !! ================ \n')
    print ('===================== AGUARDE !!! ============================== \n')

    if os.path.isdir("C:/Users/rodolfo.dona/Documents/Projetos/LOGS/"+projeto+""):
        print ('Pasta do projeto ja existe!! \n')
    else:
        print ('Criando pasta do projeto!! \n')
        os.mkdir("C:/Users/rodolfo.dona/Documents/Projetos/LOGS/"+projeto+"")
        print ('Pasta criada! \n')

    dest_arq = 'C:/Users/rodolfo.dona/Documents/Projetos/LOGS/'+projeto+'/'



    reg = result

    # DEFININDO PLANILHA
    wb = xlwt.Workbook()
    ws = wb.add_sheet('REGISTRO_WS')

    # TITULO DAS COLUNAS
    titles = ['DATA_HORA_REQUEST','PATH','DADOS_ENVIADOS','DADOS_RECEBIDOS','ID_LOG_WEB_REQUEST','DATA_HORA_REQUEST','HTTPS','PATH','METODO','CONTENT_TYPE','DADOS_ENVIADOS','ID_CLIENTE','CAMPANHA','ID_LIGACAO','ID_LOG_WEB_RESPONSE','ID_LOG_WEB_REQUEST','STATUS_SCORE','CONTENT_TYPE','DADOS_ENVIADOS','DATA_HORA_RESPONSE']

    # DEFINDO A FONTE UTILIZADA NA TABELA
    style0 =xlwt.easyxf('font: name Times New Roman, color-index red, bold on')
    # DEFININDO O FORMATO DA DATA E HORARIO
    style1 = xlwt.XFStyle()
    style1.num_format_str = 'dd/mm/yyyy hh:mm:ss'

    # ESCREVENDO O TITULO DAS COLUNAS NO ARQUIVO
    for i in range(len(titles)):
        #       lin,col,conteudo, formatacao
        ws.write(0, i, titles[i], style0)

    # Definindo largura das c�lulas das sequ�ncia
    for i in range(1,50):
        ws.col(i).width = 512
        i = i+ 1

    # INSERINDO RETORNOS DO WS NA PLANILHA
    r = 0
    l = 1
    c = 0
    for a in range(len(reg)):
        ws.write(l,c,reg[r][0], style1)
        c = c+1
        ws.write(l,c,reg[r][1])
        c = c+1
        ws.write(l,c,reg[r][2])
        c = c+1
        ws.write(l,c,reg[r][3])
        c = c+1
        ws.write(l,c,reg[r][4])
        c = c+1
        ws.write(l,c,reg[r][5], style1)
        c = c+1
        ws.write(l,c,reg[r][6])
        c = c+1
        ws.write(l,c,reg[r][7])
        c = c+1
        ws.write(l,c,reg[r][8])
        c = c+1
        ws.write(l,c,reg[r][9])
        c = c+1
        ws.write(l,c,reg[r][10])
        c = c+1
        ws.write(l,c,reg[r][11])
        c = c+1
        ws.write(l,c,reg[r][12])
        c = c+1
        ws.write(l,c,reg[r][13])
        c = c+1
        ws.write(l,c,reg[r][14])
        c = c+1
        ws.write(l,c,reg[r][15])
        c = c+1
        ws.write(l,c,reg[r][16])
        c = c+1
        ws.write(l,c,reg[r][17])
        c = c+1
        ws.write(l,c,reg[r][18])
        c = c+1
        ws.write(l,c,reg[r][19])
        c = c+1
        ws.write(l,c,reg[r][20])
        #c = c+1
        #ws.write(l,c,reg[r][21])

        l = l+1
        c = 0
        r = r+1

    wb.save(''+dest_arq+'WEB_SERVICE_ID_CLIENTE_'+str(idcliente)+'.xls')

    print ('PLANILHA GERADA COM SUCESSO!! \n\nVERIFIQUE NO DIRETORIO C:/Users/rodolfo.dona/Documents/Projetos/LOGS/'+projeto+'/WEB_SERVICE_ID_CLIENTE_'+str(idcliente)+'.xls')

    return



#def transfere_logs():
